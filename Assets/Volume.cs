﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Volume : MonoBehaviour {

    Slider volumeSlider;

    void Awake()
    {
        volumeSlider = gameObject.GetComponent<Slider>();
    }

    public void setVolume()
    {
        AudioListener.volume = volumeSlider.value;
    }
	
}
