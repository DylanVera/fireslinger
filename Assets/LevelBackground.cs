﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]


public class LevelBackground : MonoBehaviour
{
    public bool FollowCamera = true;

    public bool FollowCameraEditMode = true;

    protected CameraController _cameraController;

    protected virtual void OnEnable()
    {			
        _cameraController = FindObjectOfType<CameraController>();
    }

    protected virtual void Update()
    {
        if (_cameraController == null)
            return;

        if (FollowCamera)
        {
            if ((!FollowCameraEditMode) && (Application.isEditor))
                return;

            transform.position = new Vector3(_cameraController.transform.position.x, _cameraController.transform.position.y, transform.position.z);
        }
    }
}
