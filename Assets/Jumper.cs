﻿using UnityEngine;
using System.Collections;

public class Jumper : MonoBehaviour
{
    public float JumpPlatformBoost = 40;

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        PlayerController controller = collider.GetComponent<PlayerController>();
        if (controller == null)
            return;

        controller.SetVerticalForce(Mathf.Sqrt(2f * JumpPlatformBoost * -controller.Parameters.Gravity));
    }
}
