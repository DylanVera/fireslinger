﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class Timer : MonoBehaviour
{

    // reference to timer text
    public Text timer;
    public float seconds = 72;

    float startTime;
    float timeLeft;

    // Use this for initialization
    void Start()
    {
        startTime = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time - startTime > seconds)
        {
            GameOver();
            return;
        }
        timeLeft = Mathf.Round((seconds - (Time.time - startTime)));
        timer.text = string.Format("{0:00}:{1:00}", Mathf.FloorToInt(timeLeft / 60), timeLeft % 60);
    }

    private void GameOver()
    {
        Debug.Log("Game over");
        Debug.Log("Winner(s): " + string.Join(", ", GetWinnerTags().ToArray()));
        Application.LoadLevel("MultiplayerLevelSelection");
    }

    private List<string> GetWinnerTags()
    {
        int mostKills = Mathf.Max(MLevelManager.Instance.p1Kills, MLevelManager.Instance.p2Kills, MLevelManager.Instance.p3Kills);

        List<string> tags = new List<string>();

        if (MLevelManager.Instance.p1Kills == mostKills)
            tags.Add("Player 1");
        if (MLevelManager.Instance.p2Kills == mostKills)
            tags.Add("Player 2");
        if (MLevelManager.Instance.p3Kills == mostKills)
            tags.Add("Player 3");
        /*if (MLevelManager.Instance.p1Kill)
            tags.Add("Player 1");*/

        return tags;
    }
}
