﻿using UnityEngine;
using System.Collections;

public class Stimpack : MonoBehaviour, IPlayerRespawnListener
{
    public GameObject Effect;
    public int HealthToGive;

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        var player = collider.GetComponent<CharacterBehavior>();
        
        if (player == null)
            return;

        player.GiveHealth(HealthToGive, gameObject);
        Instantiate(Effect, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }

    public virtual void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player)
    {
        gameObject.SetActive(true);
    }
}
