﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class KillManager : MonoBehaviour
{

    public int playerNum;
    GameObject player;
    MCharacterBehavior behavior;
    Text killCount;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player " + playerNum);
        behavior = player.GetComponent<MCharacterBehavior>();
    }

    // Update is called once per frame
    void Update()
    {
        killCount.text = behavior.Kills.ToString();
    }
}