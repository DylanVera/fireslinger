﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour
{
    public string LevelName;

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.GetComponent<CharacterBehavior>() == null)
            return;

        GoToNextLevel();
    }

    public virtual void GoToNextLevel()
    {
        LevelManager.Instance.GotoLevel(LevelName);

    }
}
