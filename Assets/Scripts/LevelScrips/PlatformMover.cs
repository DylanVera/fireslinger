﻿using UnityEngine;
using System.Collections;

public class PlatformMover : MonoBehaviour {
    
    public float speed;
    public float range;

    private int direction;
    
    private float minX;
    private float maxX;

    private Rigidbody rigidBody;

	// Use this for initialization
	void Start () {
        if (Random.value > .5)
            direction = -1;
        else
            direction = 1;

        minX = transform.position.x - range / 2;
        maxX = transform.position.x + range / 2;

        rigidBody = GetComponent<Rigidbody>();
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x <= minX || transform.position.x >= maxX)
            direction *= -1;

        rigidBody.MovePosition(transform.position + Vector3.right * speed * Time.fixedDeltaTime * direction);

        //transform.Translate(Vector3.right*speed*Time.fixedDeltaTime* direction);
	}
}
