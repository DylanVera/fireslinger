﻿using UnityEngine;
using System.Collections;

public class DropdownManager : MonoBehaviour
{

    public GameObject dropdown;
    public float spawnTime = 10f;
    public Transform[] spawnPoints;

    // Use this for initialization
    void Start()
    {
        InvokeRepeating("Spawn", spawnTime, spawnTime);
    }

    void Spawn()
    {
        int spawnPointIndex = Random.Range(0, spawnPoints.Length);
        var hitColliders = Physics.OverlapSphere(spawnPoints[spawnPointIndex].position, 1);
        bool hitOtherDropdown = false;

        foreach (Collider collider in hitColliders)
            if (collider.gameObject.tag == "Dropdown")
                hitOtherDropdown = true;
        GameObject createdObject = null;
        if (!hitOtherDropdown)
            createdObject = Instantiate(dropdown, spawnPoints[spawnPointIndex].position, spawnPoints[spawnPointIndex].rotation) as GameObject;

        if (createdObject != null)
            createdObject.GetComponent<DropDownController>().type = (DropDownController.DropdownType)Random.Range(0, 3);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
