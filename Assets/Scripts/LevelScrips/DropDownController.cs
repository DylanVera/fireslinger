﻿using UnityEngine;
using System.Collections;

public class DropDownController : MonoBehaviour
{
    public enum DropdownType { PLAYER_SPEED, PLAYER_HEALTH, INCREASE_DMG }

    static int[] possibleHealthDrops = { 5, 5, 5, 5, 10, 10, 10, 15, 15, 15, 25, 25, 50 };
    static float[] possibleDmgDrops = { 1.5f, 1.5f, 1.5f, 2.0f, 2.0f, 3.0f };
    static float[] possibleSpeedDrops = { 1.5f, 1.5f, 1.5f, 2.0f, 2.0f, 3.0f };

    private float deltaValue;
    public DropdownType type;

    // Use this for initialization
    void Start()
    {
        switch (type)
        {
            case DropdownType.PLAYER_HEALTH:
                GetComponent<Renderer>().material.color = Color.red;
                deltaValue = possibleHealthDrops[Random.Range(0, possibleHealthDrops.Length)];
                break;
            case DropdownType.PLAYER_SPEED:
                GetComponent<Renderer>().material.color = Color.blue;
                deltaValue = possibleSpeedDrops[Random.Range(0, possibleSpeedDrops.Length)];
                break;
            case DropdownType.INCREASE_DMG:
                GetComponent<Renderer>().material.color = Color.green;
                deltaValue = possibleDmgDrops[Random.Range(0, possibleDmgDrops.Length)];
                break;
        }

        transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = deltaValue.ToString();
        transform.GetChild(1).gameObject.GetComponent<TextMesh>().text = deltaValue.ToString();
        transform.GetChild(2).gameObject.GetComponent<TextMesh>().text = deltaValue.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider other)
    {
        string otherTag = other.gameObject.tag;
        if (otherTag.Equals("Player 1") || otherTag.Equals("Player 2"))
        {
            switch (type)
            {
                case DropdownType.INCREASE_DMG:
                    other.gameObject.GetComponent<MPlayerController>().IncreaseDamage(deltaValue);
                    break;
                case DropdownType.PLAYER_HEALTH:
                    other.gameObject.GetComponent<MPlayerController>().IncreaseHealth(deltaValue);
                    break;
                case DropdownType.PLAYER_SPEED:
                    other.gameObject.GetComponent<MPlayerController>().IncreaseSpeed(deltaValue);
                    break;
            }
        }
    }
}
