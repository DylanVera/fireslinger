﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MPlayerController : MonoBehaviour
{
    // public variables
    public float startSpeed = 15;
    public float startHealth = 100;
    public float startDmg = 10;

    public float maxSpeed = 45;
    public float maxDmg = 50;

    public float upgradeTime = 5f;

    public GameObject PlayerHUD;

    // private variables
    Rigidbody playerRigidBody;
    Vector3 movement;

    float currentHealth;
    float currentDmg;
    float currentSpeed;

    float speedTimer;
    float dmgTimer;

    Text healthText;
    Text dmgText;
    Text speedText;

    // set up variables
    void Awake()
    {
        playerRigidBody = GetComponent<Rigidbody>();
    }

    // Use this for initialization
    void Start()
    {
        currentHealth = startHealth;
        currentDmg = startDmg;
        currentSpeed = startSpeed;

        speedTimer = upgradeTime + 1;
        dmgTimer = upgradeTime + 1;

        healthText = PlayerHUD.transform.GetChild(1).gameObject.GetComponent<Text>();
        dmgText = PlayerHUD.transform.GetChild(2).gameObject.GetComponent<Text>();
        speedText = PlayerHUD.transform.GetChild(3).gameObject.GetComponent<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        dmgTimer += Time.deltaTime;
        speedTimer += Time.deltaTime;

        if (dmgTimer > upgradeTime)
        {
            currentDmg = startDmg;
            dmgText.text = "Damage: 1x";
        }
        if (speedTimer > upgradeTime)
        {
            currentSpeed = startSpeed;
            speedText.text = "Speed: 1x";
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
    }

    void Move(float h, float v)
    {
        // crate direction for movement vector
        movement = new Vector3(h, v, 0f);

        // normalize so that motion depends on speed
        movement = movement.normalized * currentSpeed * Time.deltaTime;

        // move rigid body
        playerRigidBody.MovePosition(transform.position + movement);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Dropdown"))
        {
            Destroy(other.gameObject);
        }
    }

    public void TakeDamage(float dmg)
    {
        currentHealth -= dmg;

        if (currentHealth < 0)
            Death();

        healthText.text = "Health: " + currentHealth;
    }

    public void IncreaseHealth(float health)
    {
        currentHealth += health;

        if (currentHealth > startHealth)
            currentHealth = startHealth;

        healthText.text = "Health: " + currentHealth;
    }

    void Death()
    {
        Debug.Log(tag + " died");
    }

    public void IncreaseSpeed(float speedMultiplier)
    {
        if (speedTimer >= upgradeTime)
        {
            currentSpeed = startSpeed * speedMultiplier;

            speedTimer = 0;
            speedText.text = "Speed: " + speedMultiplier + "x";
        }
    }

    public void IncreaseDamage(float dmgMultiplier)
    {
        if (dmgTimer > upgradeTime)
        {
            currentDmg = startDmg * dmgMultiplier;
            
            dmgTimer = 0;
            dmgText.text = "Damage: " + dmgMultiplier + "x";
        }
    }
}
