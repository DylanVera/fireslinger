
public interface IMPlayerRespawnListener
{
    void onPlayerRespawnInThisCheckpoint(MCheckPoint checkpoint, MCharacterBehavior player);
}