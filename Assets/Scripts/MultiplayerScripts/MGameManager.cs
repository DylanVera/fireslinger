﻿using UnityEngine;
using System.Collections;

public class MGameManager : PersistentSingleton<MGameManager> {

    public int Points { get; private set; }
    public float TimeScale { get; private set; }
    public bool Paused { get; set; }
    public bool CanMove = true;

    public MCharacterBehavior Player1 { get; set; }
    public MCharacterBehavior Player2 { get; set; }
    public MCharacterBehavior Player3 { get; set; }
    public MCharacterBehavior Player4 { get; set; }

    protected float savedTimeScale = 1f;

  
    public virtual void Reset()
    {
        Points = 0;
        TimeScale = 1f;
        Paused = false;
        CanMove = false;
        GUIManager.Instance.RefreshPoints();
    }

    public virtual void AddPoints(int pointsToAdd)
    {
        Points += pointsToAdd;
        GUIManager.Instance.RefreshPoints();
    }

    public virtual void SetPoints(int points)
    {
        Points = points;
        GUIManager.Instance.RefreshPoints();
    }

    public virtual void SetTimeScale(float newTimeScale)
    {
        savedTimeScale = Time.timeScale;
        Time.timeScale = newTimeScale;
    }

    public virtual void ResetTimeScale()
    {
        Time.timeScale = savedTimeScale;
    }

    public virtual void Pause()
    {
        if (Time.timeScale > 0.0f)
        {
            Instance.SetTimeScale(0.0f);
            Instance.Paused = true;
            GUIManager.Instance.SetPause(true);
        }
        else
        {
            UnPause();
        }
    }

    public virtual void UnPause()
    {
        Instance.ResetTimeScale();
        Instance.Paused = false;
        if (GUIManager.Instance != null)
        {
            GUIManager.Instance.SetPause(false);
        }
    }

    public virtual void FreezeCharacter()
    {
        Player1.SetHorizontalMove(0);
        Player1.SetVerticalMove(0);
        Instance.CanMove = false;
    }
}
