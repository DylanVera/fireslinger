﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;
using UnityEngine.UI;

public class MLevelManager : MonoBehaviour
{

    public static MLevelManager Instance { get; private set; }

    [Header("Prefabs")]
    public MCharacterBehavior PlayerPrefab;
    public MCheckPoint DebugSpawn;
    public TimeSpan RunningTime { get { return DateTime.UtcNow - started; } }

    public Text p1Score;
    public Text p2Score;
    public Text p3Score;
    public Text p4Score;
    public int p1Kills { get; set; }
    public int p2Kills { get; set; }
    public int p3Kills { get; set; }
    public int p4Kills { get; set; }

    [Space(10)]
    [Header("Intro and Outro durations")]
    public float IntroFadeDuration = 1f;
    public float OutroFadeDuration = 1f;

    protected MCharacterBehavior player1;
    protected MCharacterBehavior player2;
    protected MCharacterBehavior player3;
    protected MCharacterBehavior player4;

    public Transform p1originSpawn;
    public Transform p2originSpawn;
    public Transform p3originSpawn;
    public Transform p4originSpawn;

    protected List<MCheckPoint> checkpoints;
    protected int currentCheckPointIndex;
    protected DateTime started;
    protected int savedPoints;

    public virtual void Awake()
    {
        Instance = this;
        if (PlayerPrefab != null)
        {
            player1 = (MCharacterBehavior)Instantiate(PlayerPrefab, p1originSpawn.position, Quaternion.identity);
            player1.tag = "Player 1";
            MeshRenderer gameObjectRenderer = GameObject.FindGameObjectWithTag("Player 1").GetComponent<MeshRenderer>();
            Material newMaterial = new Material(Shader.Find("Standard"));
            newMaterial.color = Color.magenta;
            gameObjectRenderer.material = newMaterial;
            // new code
            player2 = (MCharacterBehavior)Instantiate(PlayerPrefab, p2originSpawn.position, Quaternion.identity);
            player2.tag = "Player 2";
            gameObjectRenderer = GameObject.FindGameObjectWithTag("Player 2").GetComponent<MeshRenderer>();
            newMaterial = new Material(Shader.Find("Standard"));
            newMaterial.color = new Color(139f, 69f, 19f); //139 69 19
            gameObjectRenderer.material = newMaterial;

            player3 = (MCharacterBehavior)Instantiate(PlayerPrefab, p3originSpawn.position, Quaternion.identity);
            player3.tag = "Player 3";
            gameObjectRenderer = GameObject.FindGameObjectWithTag("Player 3").GetComponent<MeshRenderer>();
            newMaterial = new Material(Shader.Find("Standard"));
            newMaterial.color = Color.green;
            gameObjectRenderer.material = newMaterial;

            player4 = (MCharacterBehavior)Instantiate(PlayerPrefab, p4originSpawn.position, Quaternion.identity);
            player4.tag = "Player 4";
            gameObjectRenderer = GameObject.FindGameObjectWithTag("Player 4").GetComponent<MeshRenderer>();
            newMaterial = new Material(Shader.Find("Standard"));
            newMaterial.color = Color.blue;
            gameObjectRenderer.material = newMaterial;

            MGameManager.Instance.Player1 = player1;
            MGameManager.Instance.Player2 = player2;
            MGameManager.Instance.Player3 = player3;
            MGameManager.Instance.Player4 = player4;
        }
    }

    public virtual void Start()
    {
        savedPoints = MGameManager.Instance.Points;
        checkpoints = FindObjectsOfType<MCheckPoint>().OrderBy(t => t.transform.position.x).ToList();
        currentCheckPointIndex = checkpoints.Count > 0 ? 0 : -1;
        started = DateTime.UtcNow;

        var listeners = FindObjectsOfType<MonoBehaviour>().OfType<IMPlayerRespawnListener>();
        foreach (var listener in listeners)
        {
            for (var i = checkpoints.Count - 1; i >= 0; i--)
            {
                var distance = ((MonoBehaviour)listener).transform.position.x - checkpoints[i].transform.position.x;
                if (distance < 0)
                    continue;

                checkpoints[i].AssignObjectToCheckPoint(listener);
                break;
            }
        }

        if (GUIManager.Instance != null)
        {
            GUIManager.Instance.SetLevelName(Application.loadedLevelName);

            GUIManager.Instance.FaderOn(false, IntroFadeDuration);
        }

#if UNITYEDITOR
        if (DebugSpawn!= null)
		{
			DebugSpawn.SpawnPlayer(player);
		}
		else if (currentCheckPointIndex != -1)
		{
			checkpoints[currentCheckPointIndex].SpawnPlayer(player);
		}
#else
        if (currentCheckPointIndex != -1)
        {
            /*
            checkpoints[currentCheckPointIndex].SpawnPlayer(player1);
            checkpoints[currentCheckPointIndex].SpawnPlayer(player2);
            checkpoints[currentCheckPointIndex].SpawnPlayer(player3);
            */
        }
#endif
    }

    public virtual void Update()
    {

        p1Score.text = "Kills: "+ p1Kills.ToString();
        p2Score.text = "Kills: " + p2Kills.ToString();
        p3Score.text = "Kills: " + p3Kills.ToString();
        p4Score.text = "Kills: " + p4Kills.ToString();

        var isAtLastCheckPoint = currentCheckPointIndex + 1 >= checkpoints.Count;
        if (isAtLastCheckPoint)
            return;

        var distanceToNextCheckPoint = checkpoints[currentCheckPointIndex + 1].transform.position.x - player1.transform.position.x;
        if (distanceToNextCheckPoint >= 0)
            return;

        checkpoints[currentCheckPointIndex].PlayerLeftCheckPoint();

        currentCheckPointIndex++;
        checkpoints[currentCheckPointIndex].PlayerHitCheckPoint();

        savedPoints = MGameManager.Instance.Points;
        started = DateTime.UtcNow;
    }

    public virtual void GotoLevel(string levelName)
    {
        if (GUIManager.Instance != null)
        {
            GUIManager.Instance.FaderOn(true, OutroFadeDuration);
        }
        StartCoroutine(GotoLevelCo(levelName));
    }

    protected virtual IEnumerator GotoLevelCo(string levelName)
    {
        if (player1 != null)
        {
            player1.Disable();
        }


        if (Time.timeScale > 0.0f)
        {
            yield return new WaitForSeconds(OutroFadeDuration);
        }
        MGameManager.Instance.UnPause();

        if (string.IsNullOrEmpty(levelName))
            Application.LoadLevel("StartScreen");
        else
            Application.LoadLevel(levelName);

    }

    public virtual void KillPlayer1()
    {
        StartCoroutine(KillPlayer1Co());
    }

    public virtual void KillPlayer2()
    {
        StartCoroutine(KillPlayer2Co());
    }

    public virtual void KillPlayer3()
    {
        StartCoroutine(KillPlayer3Co());
    }

    public virtual void KillPlayer4()
    {
        StartCoroutine(KillPlayer4Co());
    }

    protected virtual IEnumerator KillPlayer1Co()
    {
        player1.Kill();
        yield return new WaitForSeconds(2f);

        if (currentCheckPointIndex != -1)
            checkpoints[currentCheckPointIndex].SpawnPlayer(player1);

        started = DateTime.UtcNow;
        MGameManager.Instance.SetPoints(savedPoints);

        DebugSpawn.SpawnPlayer(player1);

    }

    protected virtual IEnumerator KillPlayer2Co()
    {
        player2.Kill();
        yield return new WaitForSeconds(2f);

        if (currentCheckPointIndex != -1)
            checkpoints[currentCheckPointIndex].SpawnPlayer(player2);

        started = DateTime.UtcNow;
        MGameManager.Instance.SetPoints(savedPoints);

        DebugSpawn.SpawnPlayer(player2);
    }

    protected virtual IEnumerator KillPlayer3Co()
    {
        player3.Kill();
        yield return new WaitForSeconds(2f);

        if (currentCheckPointIndex != -1)
            checkpoints[currentCheckPointIndex].SpawnPlayer(player3);

        started = DateTime.UtcNow;
        MGameManager.Instance.SetPoints(savedPoints);

        DebugSpawn.SpawnPlayer(player3);
    }

    protected virtual IEnumerator KillPlayer4Co()
    {
        player4.Kill();
        yield return new WaitForSeconds(2f);

        if (currentCheckPointIndex != -1)
            checkpoints[currentCheckPointIndex].SpawnPlayer(player4);

        started = DateTime.UtcNow;
        MGameManager.Instance.SetPoints(savedPoints);

        DebugSpawn.SpawnPlayer(player4);
    }
}
