﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

public class MInputManager : PersistentSingleton<MInputManager>
{

    public bool InDialogueZone;

    protected static MCharacterBehavior player1;
    protected static MCharacterBehavior player2;
    protected static MCharacterBehavior player3;
    protected static MCharacterBehavior player4;

    protected static PlayerController controller1;
    protected static PlayerController controller2;
    protected static PlayerController controller3;
    protected static PlayerController controller4;

    protected virtual void Start()
    {
        InDialogueZone = false;
        if (MGameManager.Instance.Player1 != null)
        {
            player1 = MGameManager.Instance.Player1;
            if (MGameManager.Instance.Player1.GetComponent<PlayerController>() != null)
            {
                controller1 = MGameManager.Instance.Player1.GetComponent<PlayerController>();
            }
        }

        if (MGameManager.Instance.Player2 != null)
        {
            player2 = MGameManager.Instance.Player2;
            if (MGameManager.Instance.Player2.GetComponent<PlayerController>() != null)
            {
                controller2 = MGameManager.Instance.Player2.GetComponent<PlayerController>();
            }
        }

        if (MGameManager.Instance.Player3 != null)
        {
            player3 = MGameManager.Instance.Player3;
            if (MGameManager.Instance.Player3.GetComponent<PlayerController>() != null)
            {
                controller3 = MGameManager.Instance.Player3.GetComponent<PlayerController>();
            }
        }

        if (MGameManager.Instance.Player4 != null)
        {
            player4 = MGameManager.Instance.Player4;
            if (MGameManager.Instance.Player4.GetComponent<PlayerController>() != null)
            {
                controller4 = MGameManager.Instance.Player4.GetComponent<PlayerController>();
            }
        }
    }

    protected virtual void Update()
    {
        if (player1 == null)
        {
            if (MGameManager.Instance.Player1 != null)
            {
                if (MGameManager.Instance.Player1.GetComponent<CharacterBehavior>() != null)
                    player1 = MGameManager.Instance.Player1;
                controller1 = MGameManager.Instance.Player1.GetComponent<PlayerController>();
            }
            else
                return;
        }

        if (player2 == null)
        {
            if (MGameManager.Instance.Player2 != null)
            {
                if (MGameManager.Instance.Player2.GetComponent<CharacterBehavior>() != null)
                    player2 = MGameManager.Instance.Player2;
                controller2 = MGameManager.Instance.Player2.GetComponent<PlayerController>();
            }
            else
                return;
        }

        if (player3 == null)
        {
            if (MGameManager.Instance.Player3 != null)
            {
                if (MGameManager.Instance.Player3.GetComponent<CharacterBehavior>() != null)
                    player3 = MGameManager.Instance.Player3;
                controller3 = MGameManager.Instance.Player3.GetComponent<PlayerController>();
            }
            else
                return;
        }

        if (player4 == null)
        {
            if (MGameManager.Instance.Player4 != null)
            {
                if (MGameManager.Instance.Player4.GetComponent<CharacterBehavior>() != null)
                    player4 = MGameManager.Instance.Player4;
                controller4 = MGameManager.Instance.Player4.GetComponent<PlayerController>();
            }
            else
                return;
        }

        if (CrossPlatformInputManager.GetButtonDown("Pause"))
            MGameManager.Instance.Pause();

        if (MGameManager.Instance.Paused)
            return;

        if (!MGameManager.Instance.CanMove)
            return;

        player1.SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P1"));
        player1.SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P1"));

        player2.SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P2"));
        player2.SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P2"));

        player3.SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P3"));
        player3.SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P3"));

        player4.SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P4"));
        player4.SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P4"));

        if ((CrossPlatformInputManager.GetButtonDown("Run_P1") || CrossPlatformInputManager.GetButton("Run_P1")))
            player1.RunStart();

        if ((CrossPlatformInputManager.GetButtonDown("Run_P2") || CrossPlatformInputManager.GetButton("Run_P2")))
            player2.RunStart();

        if ((CrossPlatformInputManager.GetButtonDown("Run_P3") || CrossPlatformInputManager.GetButton("Run_P3")))
            player3.RunStart();

        if ((CrossPlatformInputManager.GetButtonDown("Run_P4") || CrossPlatformInputManager.GetButton("Run_P4")))
            player4.RunStart();

        if (CrossPlatformInputManager.GetButtonUp("Run_P1"))
            player1.RunStop();

        if (CrossPlatformInputManager.GetButtonUp("Run_P2"))
            player2.RunStop();

        if (CrossPlatformInputManager.GetButtonUp("Run_P3"))
            player3.RunStop();

        if (CrossPlatformInputManager.GetButtonUp("Run_P4"))
            player4.RunStop();

        if (CrossPlatformInputManager.GetButtonDown("Jump_P1"))
        {
            //  if (!player.BehaviorState.InDialogueZone)
            {
                player1.JumpStart();
            }
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump_P2"))
        {
            //  if (!player.BehaviorState.InDialogueZone)
            {
                player2.JumpStart();
            }
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump_P3"))
        {
            //  if (!player.BehaviorState.InDialogueZone)
            {
                player3.JumpStart();
            }
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump_P4"))
        {
            //  if (!player.BehaviorState.InDialogueZone)
            {
                player4.JumpStart();
            }
        }

        if (CrossPlatformInputManager.GetButtonUp("Jump_P1"))
        {
            player1.JumpStop();
        }

        if (CrossPlatformInputManager.GetButtonUp("Jump_P2"))
        {
            player2.JumpStop();
        }

        if (CrossPlatformInputManager.GetButtonUp("Jump_P3"))
        {
            player3.JumpStop();
        }

        if (CrossPlatformInputManager.GetButtonUp("Jump_P4"))
        {
            player4.JumpStop();
        }

        if (CrossPlatformInputManager.GetButtonDown("Dash_P1"))
            player1.Dash();

        if (CrossPlatformInputManager.GetButtonDown("Dash_P2"))
            player2.Dash();

        if (CrossPlatformInputManager.GetButtonDown("Dash_P3"))
            player3.Dash();

        if (CrossPlatformInputManager.GetButtonDown("Dash_P4"))
            player4.Dash();

        //Reimplement if we figure out a better method for melee attacks
        /*  if (player.GetComponent<CharacterMelee>() != null)
          {
              if (CrossPlatformInputManager.GetButtonDown("Melee"))
                  player.GetComponent<CharacterMelee>().Melee();
          }*/

        if (player1.GetComponent<CharacterShoot>() != null)
        {
            player1.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P1"));
            player1.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P1"));

            if (CrossPlatformInputManager.GetButtonDown("Fire_P1"))
                player1.GetComponent<CharacterShoot>().ShootOnce();

            if (CrossPlatformInputManager.GetButton("Fire_P1"))
                player1.GetComponent<CharacterShoot>().ShootStart();

            if (CrossPlatformInputManager.GetButtonUp("Fire_P1"))
                player1.GetComponent<CharacterShoot>().ShootStop();
        }

        if (player2.GetComponent<CharacterShoot>() != null)
        {
            player2.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P2"));
            player2.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P2"));

            if (CrossPlatformInputManager.GetButtonDown("Fire_P2"))
                player2.GetComponent<CharacterShoot>().ShootOnce();

            if (CrossPlatformInputManager.GetButton("Fire_P2"))
                player2.GetComponent<CharacterShoot>().ShootStart();

            if (CrossPlatformInputManager.GetButtonUp("Fire_P2"))
                player2.GetComponent<CharacterShoot>().ShootStop();
        }

        if (player3.GetComponent<CharacterShoot>() != null)
        {
            player3.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P3"));
            player3.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P3"));

            if (CrossPlatformInputManager.GetButtonDown("Fire_P3"))
                player3.GetComponent<CharacterShoot>().ShootOnce();

            if (CrossPlatformInputManager.GetButton("Fire_P3"))
                player3.GetComponent<CharacterShoot>().ShootStart();

            if (CrossPlatformInputManager.GetButtonUp("Fire_P3"))
                player3.GetComponent<CharacterShoot>().ShootStop();
        }

        if (player4.GetComponent<CharacterShoot>() != null)
        {
            player4.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal_P4"));
            player4.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical_P4"));

            if (CrossPlatformInputManager.GetButtonDown("Fire_P4"))
                player4.GetComponent<CharacterShoot>().ShootOnce();

            if (CrossPlatformInputManager.GetButton("Fire_P4"))
                player4.GetComponent<CharacterShoot>().ShootStart();

            if (CrossPlatformInputManager.GetButtonUp("Fire_P4"))
                player4.GetComponent<CharacterShoot>().ShootStop();
        }

        if (player1.GetComponent<CharacterJetpack>() != null)
        {
            if ((CrossPlatformInputManager.GetButtonDown("Jetpack_P1") || CrossPlatformInputManager.GetButton("Jetpack_P1")))
                player1.GetComponent<CharacterJetpack>().JetpackStart();

            if (CrossPlatformInputManager.GetButtonUp("Jetpack_P1"))
                player1.GetComponent<CharacterJetpack>().JetpackStop();
        }

        if (player2.GetComponent<CharacterJetpack>() != null)
        {
            if ((CrossPlatformInputManager.GetButtonDown("Jetpack_P2") || CrossPlatformInputManager.GetButton("Jetpack_P2")))
                player2.GetComponent<CharacterJetpack>().JetpackStart();

            if (CrossPlatformInputManager.GetButtonUp("Jetpack_P2"))
                player2.GetComponent<CharacterJetpack>().JetpackStop();
        }

        if (player3.GetComponent<CharacterJetpack>() != null)
        {
            if ((CrossPlatformInputManager.GetButtonDown("Jetpack_P3") || CrossPlatformInputManager.GetButton("Jetpack_P3")))
                player3.GetComponent<CharacterJetpack>().JetpackStart();

            if (CrossPlatformInputManager.GetButtonUp("Jetpack_P3"))
                player3.GetComponent<CharacterJetpack>().JetpackStop();
        }

        if (player4.GetComponent<CharacterJetpack>() != null)
        {
            if ((CrossPlatformInputManager.GetButtonDown("Jetpack_P4") || CrossPlatformInputManager.GetButton("Jetpack_P4")))
                player4.GetComponent<CharacterJetpack>().JetpackStart();

            if (CrossPlatformInputManager.GetButtonUp("Jetpack_P4"))
                player4.GetComponent<CharacterJetpack>().JetpackStop();
        }
    }
}
