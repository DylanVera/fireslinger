using UnityEngine;
using System.Collections;

/// <summary>
/// Add this class to a character so it can shoot projectiles
/// </summary>
public class CharacterShoot : MonoBehaviour
{
    public Weapon InitialWeapon;
    public Transform WeaponAttachment;
    public bool EightDirectionShooting = true;
    public bool StrictEightDirectionShooting = true;

    protected Weapon weapon;
    protected float fireTimer;

    protected float horizontalMove;
    protected float verticalMove;

    protected CharacterBehavior characterBehavior;
    protected PlayerController controller;

    protected virtual void Start()
    {
        characterBehavior = GetComponent<CharacterBehavior>();
        controller = GetComponent<PlayerController>();

        if (WeaponAttachment == null)
            WeaponAttachment = transform;

        ChangeWeapon(InitialWeapon);
    }


    public virtual void ShootOnce()
    {
        if (!characterBehavior.Permissions.ShootEnabled || characterBehavior.BehaviorState.IsDead)
            return;
        if (!characterBehavior.BehaviorState.CanShoot)
        {
            characterBehavior.BehaviorState.FiringDirection = 3;
            return;
        }

        if (!characterBehavior.BehaviorState.CanMoveFreely)
            return;

        FireProjectile();
        fireTimer = 0;
    }

    public virtual void ShootStart()
    {
        if (!characterBehavior.Permissions.ShootEnabled || characterBehavior.BehaviorState.IsDead)
            return;
        if (!characterBehavior.BehaviorState.CanShoot)
        {

            characterBehavior.BehaviorState.FiringDirection = 3;
            return;
        }

        if (!characterBehavior.BehaviorState.CanMoveFreely)
            return;

        characterBehavior.BehaviorState.FiringStop = false;
        characterBehavior.BehaviorState.Firing = true;

        weapon.SetGunFlamesEmission(true);
        weapon.SetGunShellsEmission(true);
        fireTimer += Time.deltaTime;
        if (fireTimer > weapon.FireRate)
        {
            FireProjectile();
            fireTimer = 0;
        }
    }

    public virtual void ShootStop()
    {
        if (!characterBehavior.Permissions.ShootEnabled)
            return;
        if (!characterBehavior.BehaviorState.CanShoot)
        {

            characterBehavior.BehaviorState.FiringDirection = 3;
            return;
        }
        characterBehavior.BehaviorState.FiringStop = true;
        characterBehavior.BehaviorState.Firing = false;
        characterBehavior.BehaviorState.FiringDirection = 3;
        weapon.GunFlames.enableEmission = false;
        weapon.GunShells.enableEmission = false;
    }


    public virtual void ChangeWeapon(Weapon newWeapon)
    {
        if (weapon != null)
        {
            ShootStop();
        }

        weapon = (Weapon)Instantiate(newWeapon, WeaponAttachment.transform.position, WeaponAttachment.transform.rotation);
        weapon.transform.parent = transform;

        weapon.SetGunFlamesEmission(false);
        weapon.SetGunShellsEmission(false);
    }

    protected virtual void FireProjectile()
    {
        float HorizontalShoot = horizontalMove;
        float VerticalShoot = verticalMove;

        if (weapon.ProjectileFireLocation == null)
            return;

        if (!EightDirectionShooting)
        {
            HorizontalShoot = 0;
            VerticalShoot = 0;
        }

        if (StrictEightDirectionShooting)
        {
            HorizontalShoot = Mathf.Round(HorizontalShoot);
            VerticalShoot = Mathf.Round(VerticalShoot);
        }

        float angle = Mathf.Atan2(HorizontalShoot, VerticalShoot) * Mathf.Rad2Deg;

        Vector2 direction = Vector2.up;

        if (HorizontalShoot > -0.1f && HorizontalShoot < 0.1f && VerticalShoot > -0.1f && VerticalShoot < 0.1f)
        {
            bool isFacingRight = transform.localScale.x > 0;
            angle = isFacingRight ? 90f : -90f;
        }

        if (Mathf.Abs(HorizontalShoot) < 0.1f && VerticalShoot > 0.1f)
            characterBehavior.BehaviorState.FiringDirection = 1;

        if (Mathf.Abs(HorizontalShoot) > 0.1f && VerticalShoot > 0.1f)
            characterBehavior.BehaviorState.FiringDirection = 2;

        if (Mathf.Abs(HorizontalShoot) > 0.1f && VerticalShoot < -0.1f)
            characterBehavior.BehaviorState.FiringDirection = 4;

        if (Mathf.Abs(HorizontalShoot) < 0.1f && VerticalShoot < -0.1f)
            characterBehavior.BehaviorState.FiringDirection = 5;
        if (Mathf.Abs(VerticalShoot) < 0.1f)
            characterBehavior.BehaviorState.FiringDirection = 3;


        bool facingRight = transform.localScale.x > 0;
        float horizontalDirection = facingRight ? 1f : -1f;


        direction = Quaternion.Euler(0, 0, -angle) * direction;


        weapon.GunRotationCenter.transform.rotation = Quaternion.Euler(new Vector3(0, 0, -angle * horizontalDirection + 90f));


        var projectile = (Projectile)Instantiate(weapon.Projectile, weapon.ProjectileFireLocation.position, weapon.ProjectileFireLocation.rotation);
        projectile.Initialize(gameObject, direction, controller.Speed);


        if (weapon.GunShootFx != null)
            SoundManager.Instance.PlaySound(weapon.GunShootFx, transform.position);
    }

    protected virtual Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, float angle)
    {

        angle = angle * (Mathf.PI / 180f);
        var rotatedX = Mathf.Cos(angle) * (point.x - pivot.x) - Mathf.Sin(angle) * (point.y - pivot.y) + pivot.x;
        var rotatedY = Mathf.Sin(angle) * (point.x - pivot.x) + Mathf.Cos(angle) * (point.y - pivot.y) + pivot.y;
        return new Vector3(rotatedX, rotatedY, 0);
    }


    public virtual void SetHorizontalMove(float value)
    {
        horizontalMove = value;
    }


    public virtual void SetVerticalMove(float value)
    {
        verticalMove = value;
    }

    public virtual void Flip()
    {
        if (weapon.GunShells != null)
            weapon.GunShells.transform.eulerAngles = new Vector3(weapon.GunShells.transform.eulerAngles.x, weapon.GunShells.transform.eulerAngles.y + 180, weapon.GunShells.transform.eulerAngles.z);
        if (weapon.GunFlames != null)
            weapon.GunFlames.transform.eulerAngles = new Vector3(weapon.GunFlames.transform.eulerAngles.x, weapon.GunFlames.transform.eulerAngles.y + 180, weapon.GunFlames.transform.eulerAngles.z);
    }
}
