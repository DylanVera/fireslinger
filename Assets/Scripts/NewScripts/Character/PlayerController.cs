﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;
using System.Collections.Generic;

[RequireComponent(typeof(BoxCollider2D))]
// The Playercontroller that handles the character's gravity and collisions.
// It requires a Collider2D and a rigidbody to function.
public class PlayerController : MonoBehaviour
{
    /// the various states of our character
    public ControllerState State { get; protected set; }
    /// the initial parameters
    public ControllerParameters DefaultParameters;
    /// the current parameters
    public ControllerParameters Parameters { get { return overrideParameters ?? DefaultParameters; } }

    [Space(10)]
    [Header("Collision Masks")]
    /// The layer mask the platforms are on
    public LayerMask PlatformMask = 0;

    /// The layer mask the moving platforms are on
    public LayerMask MovingPlatformMask = 0;

    /// Layer mask for one way platforms
    public LayerMask EdgeColliderPlatformMask = 0;

    /// gives you the object the character is standing on
    public GameObject StandingOn { get; protected set; }

    /// the current velocity of the character
    public Vector2 Speed { get { return speed; } }

    [Space(10)]
    [Header("Raycasting")]
    public int NumberOfHorizontalRays = 8;
    public int NumberOfVerticalRays = 8;
    public float RayOffset = 0.05f;

    public Vector3 ColliderCenter
    {
        get
        {
            Vector3 colliderCenter = Vector3.Scale(transform.localScale, boxCollider.offset);
            return colliderCenter;
        }
    }
    public Vector3 ColliderPosition
    {
        get
        {
            Vector3 colliderPosition = transform.position + ColliderCenter;
            return colliderPosition;
        }
    }
    public Vector3 ColliderSize
    {
        get
        {
            Vector3 colliderSize = Vector3.Scale(transform.localScale, boxCollider.size);
            return colliderSize;
        }
    }
    public Vector3 BottomPosition
    {
        get
        {
            Vector3 colliderBottom = new Vector3(ColliderPosition.x, ColliderPosition.y - (ColliderSize.y / 2), ColliderPosition.z);
            return colliderBottom;
        }
    }

    // parameters override storage
    protected ControllerParameters overrideParameters;
    // private local references			
    protected Vector2 speed;
    protected float fallSlowFactor;
    protected Vector2 externalForce;
    protected Vector2 newPosition;
    protected Transform transform;
    protected BoxCollider2D boxCollider;
    protected GameObject lastStandingOn;
    protected LayerMask platformMaskSave;
    protected float movingPlatformsCurrentGravity;

    protected const float largeValue = 500000f;
    protected const float smallValue = 0.0001f;
    protected const float obstacleHeightTolerance = 0.05f;
    protected const float movingPlatformsGravity = -150;

    // rays parameters
    protected Rect rayBoundsRectangle;

    protected List<RaycastHit2D> contactList;


    // initialization
    protected virtual void Awake()
    {
        transform = gameObject.transform;
        boxCollider = (BoxCollider2D)GetComponent<BoxCollider2D>();
        contactList = new List<RaycastHit2D>();
        State = new ControllerState();

        // we add the edge collider platform and moving platform masks to our initial platform mask so they can be walked on	
        platformMaskSave = PlatformMask;
        PlatformMask |= EdgeColliderPlatformMask;
        PlatformMask |= MovingPlatformMask;

        State.Reset();
        SetRaysParameters();
    }


    // Use this to add force to the character
    public virtual void AddForce(Vector2 force)
    {
        speed += force;
        externalForce += force;
    }

    // use this to set the horizontal force applied to the character
    public virtual void AddHorizontalForce(float x)
    {
        speed.x += x;
        externalForce.x += x;
    }


    //  use this to set the vertical force applied to the character
    public virtual void AddVerticalForce(float y)
    {
        speed.y += y;
        externalForce.y += y;
    }


    // Use this to set the force applied to the character
    public virtual void SetForce(Vector2 force)
    {
        speed = force;
        externalForce = force;
    }


    //  use this to set the horizontal force applied to the character
    public virtual void SetHorizontalForce(float x)
    {
        speed.x = x;
        externalForce.x = x;
    }


    //  use this to set the vertical force applied to the character
    public virtual void SetVerticalForce(float y)
    {
        speed.y = y;
        externalForce.y = y;

    }


    // Every frame, we apply the gravity to our character, then check using raycasts if an object's been hit, and modify its new position 
    // accordingly. When all the checks have been done, we apply that new position. 
    protected virtual void FixedUpdate()
    {
        contactList.Clear();

        speed.y += (Parameters.Gravity + movingPlatformsCurrentGravity) * Time.deltaTime;

        if (fallSlowFactor != 0)
        {
            speed.y *= fallSlowFactor;
        }

        newPosition = Speed * Time.deltaTime;

        State.WasGroundedLastFrame = State.IsCollidingBelow;
        State.WasTouchingTheCeilingLastFrame = State.IsCollidingAbove;
        State.Reset();

        SetRaysParameters();

        CastRaysToTheSides();
        CastRaysBelow();
        CastRaysAbove();

        transform.Translate(newPosition, Space.World);

        SetRaysParameters();

        // we compute the new speed
        if (Time.deltaTime > 0)
            speed = newPosition / Time.deltaTime;

        // we make sure the velocity doesn't exceed the MaxVelocity specified in the parameters
        Mathf.Clamp(speed.x, -Parameters.MaxVelocity.x, Parameters.MaxVelocity.x);
        Mathf.Clamp(speed.y, -Parameters.MaxVelocity.y, Parameters.MaxVelocity.y);

        // we change states depending on the outcome of the movement
        if (!State.WasGroundedLastFrame && State.IsCollidingBelow)
            State.JustGotGrounded = true;

        if (State.IsCollidingLeft || State.IsCollidingRight || State.IsCollidingBelow || State.IsCollidingRight)
        {
            OnColliderHit();
        }


        externalForce.x = 0;
        externalForce.y = 0;
    }


    // Casts rays to the sides of the character, from its center axis.
    // If we hit a wall/slope, we check its angle and move or not according to it.

    protected virtual void CastRaysToTheSides()
    {
        float movementDirection = 1;
        if ((speed.x < 0) || (externalForce.x < 0))
            movementDirection = -1;

        float horizontalRayLength = Mathf.Abs(speed.x * Time.deltaTime) + rayBoundsRectangle.width / 2 + RayOffset * 2;

        Vector2 horizontalRayCastFromBottom = new Vector2(rayBoundsRectangle.center.x,
                                                        rayBoundsRectangle.yMin + obstacleHeightTolerance);
        Vector2 horizontalRayCastToTop = new Vector2(rayBoundsRectangle.center.x,
                                                   rayBoundsRectangle.yMax - obstacleHeightTolerance);

        RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfHorizontalRays];

        for (int i = 0; i < NumberOfHorizontalRays; i++)
        {
            Vector2 rayOriginPoint = Vector2.Lerp(horizontalRayCastFromBottom, horizontalRayCastToTop, (float)i / (float)(NumberOfHorizontalRays - 1));

            if (State.WasGroundedLastFrame && i == 0)
                hitsStorage[i] = Tools.RayCast(rayOriginPoint, movementDirection * Vector2.right, horizontalRayLength, PlatformMask, true, Color.red);
            else
                hitsStorage[i] = Tools.RayCast(rayOriginPoint, movementDirection * Vector2.right, horizontalRayLength, PlatformMask & ~EdgeColliderPlatformMask, true, Color.red);

            if (hitsStorage[i].distance > 0)
            {
                float hitAngle = Mathf.Abs(Vector2.Angle(hitsStorage[i].normal, Vector2.up));

                State.SlopeAngle = hitAngle;

                if (hitAngle > Parameters.MaximumSlopeAngle)
                {
                    if (movementDirection < 0)
                        State.IsCollidingLeft = true;
                    else
                        State.IsCollidingRight = true;

                    State.SlopeAngleOK = false;

                    if (movementDirection <= 0)
                    {
                        newPosition.x = -Mathf.Abs(hitsStorage[i].point.x - horizontalRayCastFromBottom.x)
                            + rayBoundsRectangle.width / 2
                                + RayOffset * 2;
                    }
                    else
                    {
                        newPosition.x = Mathf.Abs(hitsStorage[i].point.x - horizontalRayCastFromBottom.x)
                            - rayBoundsRectangle.width / 2
                                - RayOffset * 2;
                    }

                    contactList.Add(hitsStorage[i]);
                    speed = new Vector2(0, speed.y);
                    break;
                }
            }
        }


    }


    // Every frame, we cast a number of rays below our character to check for platform collisions
    protected virtual void CastRaysBelow()
    {
        if (newPosition.y < -smallValue)
        {
            State.IsFalling = true;
        }
        else
        {
            State.IsFalling = false;
        }

        if ((Parameters.Gravity > 0) && (!State.IsFalling))
            return;

        float rayLength = rayBoundsRectangle.height / 2 + RayOffset;
        if (newPosition.y < 0)
        {
            rayLength += Mathf.Abs(newPosition.y);
        }


        Vector2 verticalRayCastFromLeft = new Vector2(rayBoundsRectangle.xMin + newPosition.x,
                                                    rayBoundsRectangle.center.y + RayOffset);
        Vector2 verticalRayCastToRight = new Vector2(rayBoundsRectangle.xMax + newPosition.x,
                                                   rayBoundsRectangle.center.y + RayOffset);

        RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfVerticalRays];
        float smallestDistance = largeValue;
        int smallestDistanceIndex = 0;
        bool hitConnected = false;

        for (int i = 0; i < NumberOfVerticalRays; i++)
        {
            Vector2 rayOriginPoint = Vector2.Lerp(verticalRayCastFromLeft, verticalRayCastToRight, (float)i / (float)(NumberOfVerticalRays - 1));

            if ((newPosition.y > 0) && (!State.WasGroundedLastFrame))
                hitsStorage[i] = Tools.RayCast(rayOriginPoint, -Vector2.up, rayLength, PlatformMask & ~EdgeColliderPlatformMask, true, Color.blue);
            else
                hitsStorage[i] = Tools.RayCast(rayOriginPoint, -Vector2.up, rayLength, PlatformMask, true, Color.blue);

            if ((Mathf.Abs(hitsStorage[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y)) < smallValue)
            {
                break;
            }

            if (hitsStorage[i])
            {
                hitConnected = true;
                if (hitsStorage[i].distance < smallestDistance)
                {
                    smallestDistanceIndex = i;
                    smallestDistance = hitsStorage[i].distance;
                }
            }
        }
        if (hitConnected)
        {

            StandingOn = hitsStorage[smallestDistanceIndex].collider.gameObject;

            //contactList.Add(hitsStorage[smallestDistanceIndex]);

            // if the character is jumping onto a (1-way) platform but not high enough, we do nothing
            if (!State.WasGroundedLastFrame && smallestDistance < rayBoundsRectangle.size.y / 2 && StandingOn.layer == LayerMask.NameToLayer("OneWayPlatforms"))
            {
                State.IsCollidingBelow = false;
                return;
            }

            State.IsFalling = false;
            State.IsCollidingBelow = true;

            newPosition.y = -Mathf.Abs(hitsStorage[smallestDistanceIndex].point.y - verticalRayCastFromLeft.y)
                + rayBoundsRectangle.height / 2
                    + RayOffset;

            if (externalForce.y > 0)
            {
                newPosition.y += speed.y * Time.deltaTime;
                State.IsCollidingBelow = false;
            }

            if (!State.WasGroundedLastFrame && speed.y > 0)
            {
                newPosition.y += speed.y * Time.deltaTime;
            }



            if (Mathf.Abs(newPosition.y) < smallValue)
                newPosition.y = 0;

            // we check if the character is standing on a moving platform
            PathFollow movingPlatform = hitsStorage[smallestDistanceIndex].collider.GetComponent<PathFollow>();
            State.OnAMovingPlatform = false;
            if (movingPlatform != null)
            {
                movingPlatformsCurrentGravity = movingPlatformsGravity;
                State.OnAMovingPlatform = true;
                transform.Translate(movingPlatform.CurrentSpeed * Time.deltaTime);
                newPosition.y = 0;
            }
            else
            {
                movingPlatformsCurrentGravity = 0;
            }
        }
        else
        {
            movingPlatformsCurrentGravity = 0;
            State.IsCollidingBelow = false;
        }


    }


    // If we're in the air and moving up, we cast rays above the character's head to check for collisions
    protected virtual void CastRaysAbove()
    {

        if (newPosition.y < 0)
            return;

        float rayLength = State.IsGrounded ? RayOffset : newPosition.y * Time.deltaTime;
        rayLength += rayBoundsRectangle.height / 2;

        bool hitConnected = false;

        Vector2 verticalRayCastStart = new Vector2(rayBoundsRectangle.xMin + newPosition.x,
                                                 rayBoundsRectangle.center.y);
        Vector2 verticalRayCastEnd = new Vector2(rayBoundsRectangle.xMax + newPosition.x,
                                               rayBoundsRectangle.center.y);

        RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfVerticalRays];
        float smallestDistance = largeValue;

        for (int i = 0; i < NumberOfVerticalRays; i++)
        {
            Vector2 rayOriginPoint = Vector2.Lerp(verticalRayCastStart, verticalRayCastEnd, (float)i / (float)(NumberOfVerticalRays - 1));
            hitsStorage[i] = Tools.RayCast(rayOriginPoint, Vector2.up, rayLength, PlatformMask & ~EdgeColliderPlatformMask, true, Color.green);


            if (hitsStorage[i])
            {
                hitConnected = true;
                if (hitsStorage[i].distance < smallestDistance)
                {
                    smallestDistance = hitsStorage[i].distance;
                }
            }

        }

        if (hitConnected)
        {
            speed.y = 0;
            newPosition.y = smallestDistance - rayBoundsRectangle.height / 2;

            if ((State.IsGrounded) && (newPosition.y < 0))
            {
                newPosition.y = 0;
            }

            State.IsCollidingAbove = true;

            if (!State.WasTouchingTheCeilingLastFrame)
            {
                newPosition.x = 0;
                speed = new Vector2(0, speed.y);
            }
        }
    }


    // Creates a rectangle with the boxcollider's size for ease of use and draws debug lines along the different raycast origin axis
    public virtual void SetRaysParameters()
    {

        rayBoundsRectangle = new Rect(boxCollider.bounds.min.x,
                                       boxCollider.bounds.min.y,
                                       boxCollider.bounds.size.x,
                                       boxCollider.bounds.size.y);

        Debug.DrawLine(new Vector2(rayBoundsRectangle.center.x, rayBoundsRectangle.yMin), new Vector2(rayBoundsRectangle.center.x, rayBoundsRectangle.yMax));
        Debug.DrawLine(new Vector2(rayBoundsRectangle.xMin, rayBoundsRectangle.center.y), new Vector2(rayBoundsRectangle.xMax, rayBoundsRectangle.center.y));



    }


    // Disables the collisions for the specified duration
    public virtual IEnumerator DisableCollisions(float duration)
    {
        // we turn the collisions off
        CollisionsOff();
        // we wait for a few seconds
        yield return new WaitForSeconds(duration);
        // we turn them on again
        CollisionsOn();
    }

    public virtual void ResetMovingPlatformsGravity()
    {
        movingPlatformsCurrentGravity = 0f;
    }

    public virtual void CollisionsOn()
    {
        PlatformMask = platformMaskSave;
        PlatformMask |= EdgeColliderPlatformMask;
        PlatformMask |= MovingPlatformMask;
    }

    public virtual void CollisionsOff()
    {
        PlatformMask = 0;
    }

    public virtual void ResetParameters()
    {
        overrideParameters = DefaultParameters;
    }

    public virtual void SlowFall(float factor)
    {
        fallSlowFactor = factor;
    }

    // Events



    // triggered when the character's raycasts collide with something 
    protected virtual void OnColliderHit()
    {
        foreach (RaycastHit2D hit in contactList)
        {
            Rigidbody2D body = hit.collider.attachedRigidbody;
            if (body == null || body.isKinematic)
                return;

            Vector3 pushDir = new Vector3(externalForce.x, 0, 0);

            body.velocity = pushDir.normalized * Parameters.Physics2DPushForce;
        }
    }


    // triggered when the character enters a collider

    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {

        PhysicsVolume2D parameters = collider.gameObject.GetComponent<PhysicsVolume2D>();
        if (parameters == null)
            return;
        // if the object we're colliding with has parameters, we apply them to our character.
        overrideParameters = parameters.ControllerParameters;
    }

    // triggered while the character stays inside another collider
    protected virtual void OnTriggerStay2D(Collider2D collider)
    {
    }


    // triggered when the character exits a collider
    protected virtual void OnTriggerExit2D(Collider2D collider)
    {
        PhysicsVolume2D parameters = collider.gameObject.GetComponent<PhysicsVolume2D>();
        if (parameters == null)
            return;

        // if the object we were colliding with had parameters, we reset our character's parameters
        overrideParameters = null;
    }


}