﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class CharacterBehaviorParameters
{
    [Header("Control Type")]
    public bool SmoothMovement = true;

    [Header("Animation Type")]
    public bool UseDefaultMecanim = true;

    [Header("Jump")]
    public float JumpHeight = 3.025f;
    public float JumpMinimumAirTime = 0.1f;
    public int NumberOfJumps = 3;
    public enum JumpBehavior
    {
        CanJumpOnGround,
        CanJumpAnywhere,
        CantJump,
        CanJumpAnywhereAnyNumberOfTimes
    }
    public JumpBehavior JumpRestrictions;
    public bool JumpIsProportionalToThePressTime = true;
    public float JumpButtonReleaseForce = 12f;

    [Space(10)]
    [Header("Speed")]
    public float MovementSpeed = 8f;
    public float CrouchSpeed = 4f;
    public float WalkSpeed = 8f;
    public float RunSpeed = 16f;
    public float LadderSpeed = 2f;

    [Space(10)]
    [Header("Dash")]
    public float DashDuration = 0.15f;
    public float DashForce = 5f;
    public float DashCooldown = 2f;

    [Space(10)]
    [Header("Walljump")]
    public float WallJumpForce = 3f;
    public float WallClingingSlowFactor = 0.6f;

    [Space(10)]
    [Header("Health")]
    public int MaxHealth = 100;
}

