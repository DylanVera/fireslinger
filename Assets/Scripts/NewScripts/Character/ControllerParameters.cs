﻿using System;
using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Collider2D))]


[Serializable]
public class ControllerParameters
{
    public Vector2 MaxVelocity = new Vector2(200f, 200f);
    public int MaxSpeed = 200;

    [Range(0, 90)]
    public float MaximumSlopeAngle = 45;

    public float Gravity = -15;

    public float SpeedAccelerationOnGround = 20f;

    public float SpeedAccelerationInAir = 5f;
    [Space(10)]
    [Header("Physics2D Interaction [Experimental]")]
    public bool Phyiscs2DInteraction = true;
    public float Physics2DPushForce = 2.0f;
}
