using UnityEngine;
using System.Collections;

public class GiveDamageToPlayer : MonoBehaviour
{
    public int DamageToGive = 10;

    protected Vector2
        lastPosition,
        velocity;

    public virtual void FixedUpdate()
    {
        velocity = (lastPosition - (Vector2)transform.position) / Time.deltaTime;
        lastPosition = transform.position;
    }

    public virtual void OnTriggerEnter2D(Collider2D collider)
    {
        var player = collider.GetComponent<CharacterBehavior>();
        if (player == null)
            return;

        if (collider.tag != "Player")
            return;


        var controller = player.GetComponent<PlayerController>();
        var totalVelocity = controller.Speed + velocity;

        controller.SetForce(new Vector2(
            -1 * Mathf.Sign(totalVelocity.x) * Mathf.Clamp(Mathf.Abs(totalVelocity.x) * 5, 10, 20),
            -1 * Mathf.Sign(totalVelocity.y) * Mathf.Clamp(Mathf.Abs(totalVelocity.y) * 2, 0, 15)
            ));
        player.TakeDamage(DamageToGive, gameObject, "");
    }
}
