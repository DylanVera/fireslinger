﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

//All logic for player movement and actions 
public class CharacterBehavior : MonoBehaviour, CanTakeDamage
{
    public BoxCollider2D HeadCollider;

    public int Health { get; set; }

    public CharacterBehaviorState BehaviorState { get; protected set; }
    public CharacterBehaviorParameters DefaultBehaviorParameters;
    public CharacterBehaviorParameters BehaviorParameters { get { return overrideBehaviorParameters ?? DefaultBehaviorParameters; } }
    public CharacterBehaviorPermissions Permissions;

    [Space(10)]
    [Header("Particle Effects")]
    public ParticleSystem TouchTheGroundEffect;
    public ParticleSystem HurtEffect;

    [Space(10)]
    [Header("Sounds")]
    public AudioClip PlayerJumpSfx;
    public AudioClip PlayerHitSfx;

    public bool JumpAuthorized
    {
        get
        {
            if ((BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhere) || (BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhereAnyNumberOfTimes))
                return true;

            if (BehaviorParameters.JumpRestrictions == CharacterBehaviorParameters.JumpBehavior.CanJumpOnGround)
                return controller.State.IsGrounded;

            return false;
        }
    }

    protected CameraController sceneCamera;
    protected PlayerController controller;

    protected Animator animator;
    protected CharacterJetpack jetpack;
    protected CharacterShoot shoot;
    protected Color initialColor;
    protected Vector3 initialScale;

    protected CharacterBehaviorParameters overrideBehaviorParameters;
    protected float originalGravity;

    protected float normalizedHorizontalSpeed;

    protected float jumpButtonPressTime = 0;
    protected bool jumpButtonPressed = false;
    protected bool jumpButtonReleased = false;

    protected bool isFacingRight = true;

    protected float horizontalMove;
    protected float verticalMove;


    protected virtual void Awake()
    {
        BehaviorState = new CharacterBehaviorState();
        sceneCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();
        controller = GetComponent<PlayerController>();
        jetpack = GetComponent<CharacterJetpack>();
        shoot = GetComponent<CharacterShoot>();
        initialScale = transform.localScale;
        Health = BehaviorParameters.MaxHealth;

        if (GetComponent<Renderer>() != null)
            initialColor = GetComponent<Renderer>().material.color;
    }


    protected virtual void Start()
    {

        if (GetComponent<Animator>() != null)
        {
            animator = GetComponent<Animator>();
        }

        isFacingRight = transform.localScale.x > 0;

        originalGravity = controller.Parameters.Gravity;


        BehaviorState.Initialize();
        BehaviorState.NumberOfJumpsLeft = BehaviorParameters.NumberOfJumps;


        BehaviorState.CanJump = true;
    }


    protected virtual void Update()
    {
        
        if (!BehaviorState.IsDead)
        {
            GravityActive(true);


            HorizontalMovement();
            VerticalMovement();

            BehaviorState.CanShoot = true;


            ClimbLadder();
            WallClinging();


            if (BehaviorState.Dashing)
            {
                GravityActive(false);
                controller.SetVerticalForce(0);
            }

            if (!BehaviorState.Firing)
            {
                BehaviorState.FiringStop = false;
            }

            if (Permissions.JumpEnabled)
            {

                if ((jumpButtonPressTime != 0)
                    && (Time.time - jumpButtonPressTime >= BehaviorParameters.JumpMinimumAirTime)
                    && (controller.Speed.y > Mathf.Sqrt(Mathf.Abs(controller.Parameters.Gravity)))
                    && (jumpButtonReleased)
                    && (!jumpButtonPressed || BehaviorState.Jetpacking))
                {
                    jumpButtonReleased = false;
                    if (BehaviorParameters.JumpIsProportionalToThePressTime)
                        controller.AddForce(new Vector2(0, BehaviorParameters.JumpButtonReleaseForce * -Mathf.Abs(controller.Parameters.Gravity) * Time.deltaTime));
                }
            }
        }
        else
        {
            controller.SetHorizontalForce(0);
        }
    }


    protected virtual void FixedUpdate()
    {
        // if the character became grounded this frame, we reset the doubleJump flag so he can doubleJump again
        if (controller.State.JustGotGrounded)
        {
            BehaviorState.NumberOfJumpsLeft = BehaviorParameters.NumberOfJumps;
        }

    }



    public virtual void SetHorizontalMove(float value)
    {
        horizontalMove = value;
    }


    public virtual void SetVerticalMove(float value)
    {
        verticalMove = value;
    }

    protected virtual void HorizontalMovement()
    {
        if (!BehaviorState.CanMoveFreely)
            return;

        if (horizontalMove > 0.1f)
        {
            normalizedHorizontalSpeed = horizontalMove;
            if (!isFacingRight)
                Flip();
        }
        else if (horizontalMove < -0.1f)
        {
            normalizedHorizontalSpeed = horizontalMove;
            if (isFacingRight)
                Flip();
        }
        else
        {
            normalizedHorizontalSpeed = 0;
        }


        var movementFactor = controller.State.IsGrounded ? controller.Parameters.SpeedAccelerationOnGround : controller.Parameters.SpeedAccelerationInAir;
        if (BehaviorParameters.SmoothMovement)
            controller.SetHorizontalForce(Mathf.Lerp(controller.Speed.x, normalizedHorizontalSpeed * BehaviorParameters.MovementSpeed, Time.deltaTime * movementFactor));
        else
            controller.SetHorizontalForce(normalizedHorizontalSpeed * BehaviorParameters.MovementSpeed);
    }

    protected virtual void VerticalMovement()
    {


        if ((verticalMove > 0) && (controller.State.IsGrounded))
        {
            BehaviorState.LookingUp = true;
            sceneCamera.LookUp();
        }
        else
        {
            BehaviorState.LookingUp = false;
            sceneCamera.ResetLookUpDown();
        }

        if (controller.State.JustGotGrounded)
        {
            if (TouchTheGroundEffect != null)
            {
                Instantiate(TouchTheGroundEffect, controller.BottomPosition, transform.rotation);
            }
        }

        if (!BehaviorState.CanMoveFreely)
            return;


        if ((verticalMove < -0.1) && (controller.State.IsGrounded) && (Permissions.CrouchEnabled))
        {
            BehaviorState.Crouching = true;
            BehaviorParameters.MovementSpeed = BehaviorParameters.CrouchSpeed;
            BehaviorState.Running = false;
            sceneCamera.LookDown();
        }
        else
        {

            if (BehaviorState.Crouching)
            {
                if (HeadCollider == null)
                {
                    BehaviorState.Crouching = false;
                    return;
                }
                bool headCheck = Physics2D.OverlapCircle(HeadCollider.transform.position, HeadCollider.size.x / 2, controller.PlatformMask);

                if (!headCheck)
                {
                    if (!BehaviorState.Running)
                        BehaviorParameters.MovementSpeed = BehaviorParameters.WalkSpeed;
                    BehaviorState.Crouching = false;
                    BehaviorState.CanJump = true;
                }
                else
                {

                    BehaviorState.CanJump = false;
                }
            }
        }

        if (BehaviorState.CrouchingPreviously != BehaviorState.Crouching)
        {
            Invoke("RecalculateRays", Time.deltaTime * 10);
        }

        BehaviorState.CrouchingPreviously = BehaviorState.Crouching;


    }


    public virtual void RecalculateRays()
    {
        controller.SetRaysParameters();
    }


    public virtual void RunStart()
    {

        if (!Permissions.RunEnabled)
            return;

        if (!BehaviorState.CanMoveFreely)
            return;


        if (controller.State.IsGrounded && !BehaviorState.Crouching)
        {
            BehaviorParameters.MovementSpeed = BehaviorParameters.RunSpeed;
            BehaviorState.Running = true;
        }
    }

    public virtual void RunStop()
    {

        BehaviorParameters.MovementSpeed = BehaviorParameters.WalkSpeed;
        BehaviorState.Running = false;
    }


    public virtual void JumpStart()
	{
		if (!Permissions.JumpEnabled  || !JumpAuthorized || BehaviorState.IsDead || controller.State.IsCollidingAbove)
			return;
		
		if (controller.State.IsGrounded 
		    || BehaviorState.LadderClimbing 
		    || BehaviorState.WallClinging 
		    || BehaviorState.NumberOfJumpsLeft>0) 	
			BehaviorState.CanJump=true;
		else
			BehaviorState.CanJump=false;
					
		if ( (!BehaviorState.CanJump) && !(BehaviorParameters.JumpRestrictions==CharacterBehaviorParameters.JumpBehavior.CanJumpAnywhereAnyNumberOfTimes) )
			return;
		
		if (verticalMove<0 && controller.State.IsGrounded)
		{
			if (controller.StandingOn.layer==LayerMask.NameToLayer("OneWayPlatforms"))
			{
				
				controller.transform.position=new Vector2(transform.position.x,transform.position.y-0.1f);
				StartCoroutine(controller.DisableCollisions(0.3f));
				controller.ResetMovingPlatformsGravity();
				return;
			}
		}
		
		
		if (verticalMove>=0 && controller.State.IsGrounded)
		{
			if (controller.StandingOn.layer==LayerMask.NameToLayer("MovingPlatforms"))
			{
				StartCoroutine(controller.DisableCollisions(0.3f));
				controller.ResetMovingPlatformsGravity();
			}
		}
		
		BehaviorState.NumberOfJumpsLeft=BehaviorState.NumberOfJumpsLeft-1;
		BehaviorState.LadderClimbing=false;
		BehaviorState.CanMoveFreely=true;
		GravityActive(true);
		
		jumpButtonPressTime=Time.time;
		jumpButtonPressed=true;
		jumpButtonReleased=false;
				
		controller.SetVerticalForce(Mathf.Sqrt( 2f * BehaviorParameters.JumpHeight * Mathf.Abs(controller.Parameters.Gravity) ));
		
		if (PlayerJumpSfx!=null)
			SoundManager.Instance.PlaySound(PlayerJumpSfx,transform.position);
		
		float wallJumpDirection;
		if (BehaviorState.WallClinging)
		{
			
			if (controller.State.IsCollidingRight)
			{
				wallJumpDirection=-1f;
			}
			else
			{					
				wallJumpDirection=1f;
			}
			controller.SetForce(new Vector2(wallJumpDirection*BehaviorParameters.WallJumpForce,Mathf.Sqrt( 2f * BehaviorParameters.JumpHeight * Mathf.Abs(controller.Parameters.Gravity) )));
			BehaviorState.WallClinging=false;
		}	
		
	}

    public virtual void JumpStop()
    {
        jumpButtonPressed = false;
        jumpButtonReleased = true;
    }

    protected virtual void ClimbLadder()
    {
        if (BehaviorState.LadderColliding)
        {

            if (verticalMove > 0.1 && !BehaviorState.LadderClimbing && !BehaviorState.LadderTopColliding && !BehaviorState.Jetpacking)
            {
                BehaviorState.LadderClimbing = true;
                controller.CollisionsOn();

                BehaviorState.CanMoveFreely = false;
                if (shoot != null)
                    shoot.ShootStop();

                BehaviorState.LadderClimbingSpeed = 0;

                controller.SetHorizontalForce(0);
                controller.SetVerticalForce(0);

                GravityActive(false);
            }


            if (BehaviorState.LadderClimbing)
            {

                BehaviorState.CanShoot = false;

                GravityActive(false);

                if (!BehaviorState.LadderTopColliding)
                    controller.CollisionsOn();


                controller.SetVerticalForce(verticalMove * BehaviorParameters.LadderSpeed);

                BehaviorState.LadderClimbingSpeed = Mathf.Abs(verticalMove);
            }

            if (!BehaviorState.LadderTopColliding)
            {
                controller.CollisionsOn();
            }


            if (BehaviorState.LadderClimbing && controller.State.IsGrounded && !BehaviorState.LadderTopColliding)
            {

                BehaviorState.LadderColliding = false;
                BehaviorState.LadderClimbing = false;
                BehaviorState.CanMoveFreely = true;
                BehaviorState.LadderClimbingSpeed = 0;
                GravityActive(true);
            }
        }


        if (BehaviorState.LadderTopColliding && verticalMove < -0.1 && !BehaviorState.LadderClimbing && controller.State.IsGrounded)
        {
            controller.CollisionsOff();

            transform.position = new Vector2(transform.position.x, transform.position.y - 0.1f);

            BehaviorState.LadderClimbing = true;
            BehaviorState.CanMoveFreely = false;
            BehaviorState.LadderClimbingSpeed = 0;
            controller.SetHorizontalForce(0);
            controller.SetVerticalForce(0);
            GravityActive(false);
        }
    }


    public virtual void Dash()
    {

        float dashDirection;
        float boostForce;


        if (!Permissions.DashEnabled || BehaviorState.IsDead)
            return;

        if (!BehaviorState.CanMoveFreely)
            return;



        if (verticalMove > -0.8)
        {

            if (BehaviorState.CanDash)
            {

                BehaviorState.Dashing = true;


                if (isFacingRight)
                {
                    dashDirection = 1f;
                }
                else
                {
                    dashDirection = -1f;
                }
                boostForce = dashDirection * BehaviorParameters.DashForce;
                BehaviorState.CanDash = false;

                StartCoroutine(Boost(BehaviorParameters.DashDuration, boostForce, 0, "dash"));
            }
        }

        if (verticalMove < -0.8)
        {
            controller.CollisionsOn();

            StartCoroutine(Dive());
        }

    }


    protected virtual IEnumerator Boost(float boostDuration, float boostForceX, float boostForceY, string name)
    {
        float time = 0f;


        while (boostDuration > time)
        {

            if (boostForceX != 0)
            {
                controller.AddForce(new Vector2(boostForceX, 0));
            }
            if (boostForceY != 0)
            {
                controller.AddForce(new Vector2(0, boostForceY));
            }
            time += Time.deltaTime;

            yield return 0;
        }

        if (name == "dash")
        {
            BehaviorState.Dashing = false;
            GravityActive(true);
            yield return new WaitForSeconds(BehaviorParameters.DashCooldown);
            BehaviorState.CanDash = true;
        }
        if (name == "wallJump")
        {

        }
    }


    protected virtual IEnumerator Dive()
    {

        Vector3 ShakeParameters = new Vector3(1.5f, 0.5f, 1f);
        BehaviorState.Diving = true;

        while (!controller.State.IsGrounded)
        {
            controller.SetVerticalForce(-Mathf.Abs(controller.Parameters.Gravity) * 2);
            yield return 0; //go to next frame
        }

        sceneCamera.Shake(ShakeParameters);
        BehaviorState.Diving = false;
    }


    protected virtual void WallClinging()
    {

        if (!Permissions.WallClingingEnabled)
            return;

        if (!controller.State.IsCollidingLeft && !controller.State.IsCollidingRight)
        {
            BehaviorState.WallClinging = false;
        }


        if (!BehaviorState.CanMoveFreely)
            return;



        if ((!controller.State.IsGrounded) && (((controller.State.IsCollidingRight) && (horizontalMove > 0.1f)) || ((controller.State.IsCollidingLeft) && (horizontalMove < -0.1f))))
        {
            if (controller.Speed.y < 0)
            {
                BehaviorState.WallClinging = true;
                controller.SlowFall(BehaviorParameters.WallClingingSlowFactor);
            }
        }
        else
        {
            BehaviorState.WallClinging = false;
            controller.SlowFall(0f);
        }
    }


    protected virtual void GravityActive(bool state)
    {
        if (state == true)
        {
            if (controller.Parameters.Gravity == 0)
            {
                controller.Parameters.Gravity = originalGravity;
            }
        }
        else
        {
            if (controller.Parameters.Gravity != 0)
                originalGravity = controller.Parameters.Gravity;
            controller.Parameters.Gravity = 0;
        }
    }


    protected virtual IEnumerator Flicker(Color initialColor, Color flickerColor, float flickerSpeed)
    {
        if (GetComponent<Renderer>() != null)
        {
            for (var n = 0; n < 10; n++)
            {
                GetComponent<Renderer>().material.color = initialColor;
                yield return new WaitForSeconds(flickerSpeed);
                GetComponent<Renderer>().material.color = flickerColor;
                yield return new WaitForSeconds(flickerSpeed);
            }
            GetComponent<Renderer>().material.color = initialColor;
        }
    }


    protected virtual IEnumerator ResetLayerCollision(float delay)
    {
        yield return new WaitForSeconds(delay);
        Physics2D.IgnoreLayerCollision(9, 12, false);
        Physics2D.IgnoreLayerCollision(9, 13, false);
    }

    public virtual void Kill()
    {

        controller.CollisionsOff();
        GetComponent<Collider2D>().enabled = false;

        BehaviorState.IsDead = true;

        Health = 0;

        controller.ResetParameters();
        ResetParameters();
        controller.SetForce(new Vector2(0, 10));
    }

    public virtual void Disable()
    {
        enabled = false;
        controller.enabled = false;
        GetComponent<Collider2D>().enabled = false;
    }

    public virtual void RespawnAt(Transform spawnPoint)
    {

        if (!isFacingRight)
        {
            Flip();
        }

        BehaviorState.IsDead = false;

        GetComponent<Collider2D>().enabled = true;

        controller.CollisionsOn();
        transform.position = spawnPoint.position;
        Health = BehaviorParameters.MaxHealth;
    }

    public virtual void TakeDamage(int damage, GameObject instigator, string killerTag)
    {

        if (PlayerHitSfx != null)
            SoundManager.Instance.PlaySound(PlayerHitSfx, transform.position);


        if (HurtEffect != null)
        {
            Instantiate(HurtEffect, transform.position, transform.rotation);
        }

        Physics2D.IgnoreLayerCollision(9, 12, true);
        Physics2D.IgnoreLayerCollision(9, 13, true);
        StartCoroutine(ResetLayerCollision(0.5f));

        if (GetComponent<Renderer>() != null)
        {
            Color flickerColor = new Color32(255, 20, 20, 255);
            StartCoroutine(Flicker(initialColor, flickerColor, 0.05f));
        }

        Health -= damage;
        if (Health <= 0)
        {
            LevelManager.Instance.KillPlayer();
        }
    }


    public virtual void GiveHealth(int health, GameObject instigator)
    {
        Health = Mathf.Min(Health + health, BehaviorParameters.MaxHealth);
    }

    protected virtual void Flip()
    {

        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        isFacingRight = transform.localScale.x > 0;


        if (jetpack != null)
        {
            if (jetpack.Jetpack != null)
                jetpack.Jetpack.transform.eulerAngles = new Vector3(jetpack.Jetpack.transform.eulerAngles.x, jetpack.Jetpack.transform.eulerAngles.y + 180, jetpack.Jetpack.transform.eulerAngles.z);
        }
        if (shoot != null)
        {
            shoot.Flip();
        }
    }




    public virtual void ResetParameters()
    {
        overrideBehaviorParameters = DefaultBehaviorParameters;
    }


    public void OnTriggerEnter2D(Collider2D collider)
    {

        var parameters = collider.gameObject.GetComponent<PhysicsVolume2D>();
        if (parameters == null)
            return;

        overrideBehaviorParameters = parameters.BehaviorParameters;
    }


    public virtual void OnTriggerStay2D(Collider2D collider)
    {
    }


    public virtual void OnTriggerExit2D(Collider2D collider)
    {

        var parameters = collider.gameObject.GetComponent<PhysicsVolume2D>();
        if (parameters == null)
            return;


        overrideBehaviorParameters = null;
    }
}
