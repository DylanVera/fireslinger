using UnityEngine;
using System.Collections;

public class PlayerBounds : MonoBehaviour
{
    public enum BoundsBehavior
    {
        Nothing,
        Constrain,
        Kill
    }

    public BoundsBehavior Above;

    public BoundsBehavior Below;

    public BoundsBehavior Left;

    public BoundsBehavior Right;

    protected BoxCollider2D bounds;
    protected CharacterBehavior player;
    protected BoxCollider2D boxCollider;


    public virtual void Start()
    {
        player = GetComponent<CharacterBehavior>();
        boxCollider = GetComponent<BoxCollider2D>();
        bounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<BoxCollider2D>();
    }


    public virtual void Update()
    {

        if (player.BehaviorState.IsDead)
            return;


        var colliderSize = new Vector2(
            boxCollider.size.x * Mathf.Abs(transform.localScale.x),
            boxCollider.size.y * Mathf.Abs(transform.localScale.y)) / 2;


        if (Above != BoundsBehavior.Nothing && transform.position.y + colliderSize.y > bounds.bounds.max.y)
            ApplyBoundsBehavior(Above, new Vector2(transform.position.x, bounds.bounds.max.y - colliderSize.y));

        if (Below != BoundsBehavior.Nothing && transform.position.y - colliderSize.y < bounds.bounds.min.y)
            ApplyBoundsBehavior(Below, new Vector2(transform.position.x, bounds.bounds.min.y + colliderSize.y));

        if (Right != BoundsBehavior.Nothing && transform.position.x + colliderSize.x > bounds.bounds.max.x)
            ApplyBoundsBehavior(Right, new Vector2(bounds.bounds.max.x - colliderSize.x, transform.position.y));

        if (Left != BoundsBehavior.Nothing && transform.position.x - colliderSize.x < bounds.bounds.min.x)
            ApplyBoundsBehavior(Left, new Vector2(bounds.bounds.min.x + colliderSize.x, transform.position.y));

    }


    protected virtual void ApplyBoundsBehavior(BoundsBehavior behavior, Vector2 constrainedPosition)
    {
        if (behavior == BoundsBehavior.Kill)
        {
            LevelManager.Instance.KillPlayer();
        }
        transform.position = constrainedPosition;
    }
}
