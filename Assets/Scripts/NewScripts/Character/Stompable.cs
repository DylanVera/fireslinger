﻿using UnityEngine;
using System.Collections;
[RequireComponent(typeof(BoxCollider2D))]
[RequireComponent(typeof(Health))]

public class Stompable : MonoBehaviour
{
    public int NumberOfRays = 5;
    public float KnockbackForce = 15f;
    public LayerMask PlayerMask;
    public int DamagePerStomp;

    protected BoxCollider2D boxCollider;
    protected Health health;

    protected virtual void Start()
    {
        boxCollider = (BoxCollider2D)GetComponent<BoxCollider2D>();
        health = (Health)GetComponent<Health>();
    }

    protected virtual void Update()
    {
        CastRaysAbove();
    }

    protected virtual void CastRaysAbove()
    {
        float rayLength = 0.5f;

        bool hitConnected = false;
        int hitConnectedIndex = 0;

        Vector2 verticalRayCastStart = new Vector2(boxCollider.bounds.min.x,
                                                    boxCollider.bounds.max.y);
        Vector2 verticalRayCastEnd = new Vector2(boxCollider.bounds.max.x,
                                                   boxCollider.bounds.max.y);

        RaycastHit2D[] hitsStorage = new RaycastHit2D[NumberOfRays];

        for (int i = 0; i < NumberOfRays; i++)
        {
            Vector2 rayOriginPoint = Vector2.Lerp(verticalRayCastStart, verticalRayCastEnd, (float)i / (float)(NumberOfRays - 1));
            hitsStorage[i] = Tools.RayCast(rayOriginPoint, Vector2.up, rayLength, PlayerMask, true, Color.black);

            if (hitsStorage[i])
            {
                hitConnected = true;
                hitConnectedIndex = i;
                break;
            }
        }

        if (hitConnected)
        {
            PlayerController controller = hitsStorage[hitConnectedIndex].collider.GetComponent<PlayerController>();
            if (controller != null)
            {
                controller.SetVerticalForce(KnockbackForce);
                health.TakeDamage(DamagePerStomp, gameObject, "");
            }
        }
    }
}
