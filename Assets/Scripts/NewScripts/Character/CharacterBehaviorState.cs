using UnityEngine;
using System.Collections;

public class CharacterBehaviorState 
{
	public bool CanJump{get;set;}	
	public bool CanShoot{get;set;}	
	public bool CanMelee{get;set;}		
	public bool CanDash{get;set;}
	public bool CanMoveFreely{get;set;}
	public bool CanJetpack{ get; set; }
	public int NumberOfJumpsLeft;
	public bool Dashing{get;set;}
	public bool Running{get;set;}
	public bool Crouching{get;set;}
	public bool CrouchingPreviously{get;set;}
	public bool LookingUp{get;set;}
	public bool WallClinging{get;set;}
	public bool Jetpacking{get;set;}
	public bool Diving{get;set;}
	public bool Firing{get;set;}
	public bool FiringStop{get;set;}
	public bool MeleeAttacking{get;set;}
	public bool LadderColliding{get;set;}
	public bool LadderTopColliding{get;set;}
	public bool LadderClimbing{get;set;}
	public float LadderClimbingSpeed{get;set;}
	public int FiringDirection{get;set;}
	public bool IsDead{get;set;}
	public float JetpackFuelDurationLeft{get;set;}


	
	public virtual void Initialize()
	{				
		CanMoveFreely = true;
		CanDash = true;
		CanShoot = true;
		CanMelee = true;
		CanJetpack = true;
		Dashing = false;
		Running = false;
		Crouching = false;
		CrouchingPreviously=false;
		LookingUp = false;
		WallClinging = false;
		Jetpacking = false;
		Diving = false;
		LadderClimbing=false;
		LadderColliding=false;
		LadderTopColliding=false;
		LadderClimbingSpeed=0f;
		Firing = false;
		FiringStop = false;
		FiringDirection = 3;
		MeleeAttacking=false;

	}	
}
