﻿using UnityEngine;
using System.Collections;


public interface CanTakeDamage
{
    void TakeDamage(int damage, GameObject instigator, string killerTag);
}
