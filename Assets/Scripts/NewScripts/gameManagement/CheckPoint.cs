using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPoint : MonoBehaviour
{
    protected List<IPlayerRespawnListener> listeners;


    protected virtual void Awake()
    {
        listeners = new List<IPlayerRespawnListener>();
    }

    public virtual void PlayerHitCheckPoint()
    {

    }

    protected virtual IEnumerator PlayerHitCheckPointCo(int bonus)
    {
        yield break;
    }

    public virtual void PlayerLeftCheckPoint()
    {

    }

    public virtual void SpawnPlayer(CharacterBehavior player)
    {
        player.RespawnAt(transform);

        foreach (var listener in listeners)
        {
            listener.onPlayerRespawnInThisCheckpoint(this, player);
        }
    }

    public virtual void AssignObjectToCheckPoint(IPlayerRespawnListener listener)
    {
        listeners.Add(listener);
    }
}
