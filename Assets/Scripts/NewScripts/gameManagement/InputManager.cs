using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

public class InputManager : PersistentSingleton<InputManager>
{

    public bool InDialogueZone;
    protected static CharacterBehavior player;
    protected static PlayerController controller;

    protected virtual void Start()
    {
        InDialogueZone = false;
        if (GameManager.Instance.Player != null)
        {
            player = GameManager.Instance.Player;
            if (GameManager.Instance.Player.GetComponent<PlayerController>() != null)
            {
                controller = GameManager.Instance.Player.GetComponent<PlayerController>();
            }
        }
    }

    protected virtual void Update()
    {
        if (player == null)
        {
            if (GameManager.Instance.Player != null)
            {
                if (GameManager.Instance.Player.GetComponent<CharacterBehavior>() != null)
                    player = GameManager.Instance.Player;
                controller = GameManager.Instance.Player.GetComponent<PlayerController>();
            }
            else
                return;
        }

        if (CrossPlatformInputManager.GetButtonDown("Pause"))
            GameManager.Instance.Pause();

        if (GameManager.Instance.Paused)
            return;

        if (!GameManager.Instance.CanMove)
            return;

        player.SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal"));
        player.SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical"));

        if ((CrossPlatformInputManager.GetButtonDown("Run") || CrossPlatformInputManager.GetButton("Run")))
            player.RunStart();

        if (CrossPlatformInputManager.GetButtonUp("Run"))
            player.RunStop();


        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            
                player.JumpStart();
            
        }

        if (CrossPlatformInputManager.GetButtonUp("Jump"))
        {
            player.JumpStop();
        }

        if (CrossPlatformInputManager.GetButtonDown("Dash"))
            player.Dash();

        if (player.GetComponent<CharacterJetpack>() != null)
        {
            if ((CrossPlatformInputManager.GetButtonDown("Jetpack") || CrossPlatformInputManager.GetButton("Jetpack")))
                player.GetComponent<CharacterJetpack>().JetpackStart();

            if (CrossPlatformInputManager.GetButtonUp("Jetpack"))
                player.GetComponent<CharacterJetpack>().JetpackStop();
        }


        if (player.GetComponent<CharacterShoot>() != null)
        {
            player.GetComponent<CharacterShoot>().SetHorizontalMove(CrossPlatformInputManager.GetAxis("Horizontal"));
            player.GetComponent<CharacterShoot>().SetVerticalMove(CrossPlatformInputManager.GetAxis("Vertical"));

            if (CrossPlatformInputManager.GetButtonDown("Fire"))
                player.GetComponent<CharacterShoot>().ShootOnce();

            if (CrossPlatformInputManager.GetButton("Fire"))
                player.GetComponent<CharacterShoot>().ShootStart();

            if (CrossPlatformInputManager.GetButtonUp("Fire"))
                player.GetComponent<CharacterShoot>().ShootStop();

        }
    }
}
