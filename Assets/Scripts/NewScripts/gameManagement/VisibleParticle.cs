﻿using UnityEngine;
using System.Collections;

public class VisibleParticle : MonoBehaviour
{

    protected virtual void Start()
    {
        GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "VisibleParticles";
    }

}
