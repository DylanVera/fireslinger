﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : MonoBehaviour
{
    public float TimeBeforeFall = 2f;
    public float FallSpeed = 2f;
    public float ShakeIntensity = 2f;
    protected bool _shaking = false;
    protected bool _falling = false;
    protected Vector2 _newPosition;
    protected BoxCollider2D _bounds;


    protected virtual void Start()
    {
        
        _bounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<BoxCollider2D>();
    }

    protected virtual void FixedUpdate()
    {	

        if (TimeBeforeFall < 0)
        {
            _newPosition = new Vector2(0, -FallSpeed * Time.deltaTime);

            transform.Translate(_newPosition, Space.World);

            if (transform.position.y < _bounds.bounds.min.y)
            {
                Destroy(gameObject);
            }
        }
    }

    public virtual void OnTriggerStay2D(Collider2D collider)
    {
        PlayerController controller = collider.GetComponent<PlayerController>();
        if (controller == null)
            return;

        if (TimeBeforeFall > 0)
        {
            if (collider.transform.position.y > transform.position.y)
            {
                TimeBeforeFall -= Time.deltaTime;
                _shaking = true;
            }
        }
        else
        {
            _falling = true;
            _shaking = false;
        }
    }

    protected virtual void OnTriggerExit2D(Collider2D collider)
    {
        PlayerController controller = collider.GetComponent<PlayerController>();
        if (controller == null)
            return;

        _shaking = false;
    }
}
