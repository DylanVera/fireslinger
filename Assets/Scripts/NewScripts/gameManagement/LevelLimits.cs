﻿using UnityEngine;
using System.Collections;

public class LevelLimits : MonoBehaviour
{
    public float LeftLimit;
    public float RightLimit;
    public float BottomLimit;
    public float TopLimit;
    protected BoxCollider2D collider;

    protected virtual void Awake()
    {
        collider = GetComponent<BoxCollider2D>();

        LeftLimit = collider.bounds.min.x;
        RightLimit = collider.bounds.max.x;
        BottomLimit = collider.bounds.min.y;
        TopLimit = collider.bounds.max.y;
    }
}