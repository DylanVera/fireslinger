﻿using UnityEngine;
using System.Collections;

public class AutoDestroyParticleSystem : MonoBehaviour
{
    public bool DestroyParent = false;

    protected ParticleSystem particleSystem;

 
    protected virtual void Start()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

  
    protected virtual void Update()
    {
        if (particleSystem.isPlaying)
            return;

        if (transform.parent != null)
        {
            if (DestroyParent)
            {
                Destroy(transform.parent.gameObject);
            }
        }

        Destroy(gameObject);
    }

}
