﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MCheckPoint : MonoBehaviour {

    protected List<IMPlayerRespawnListener> listeners;

    public Transform[] possibleSpawns = new Transform[6];

    protected virtual void Awake()
    {
        listeners = new List<IMPlayerRespawnListener>();
    }

    public virtual void PlayerHitCheckPoint()
    {

    }

    protected virtual IEnumerator PlayerHitCheckPointCo(int bonus)
    {
        yield break;
    }

    public virtual void PlayerLeftCheckPoint()
    {

    }

    public virtual void SpawnPlayer(MCharacterBehavior player)
    {
        Transform spawnPoint = possibleSpawns[Random.Range(0, 4)];

        player.RespawnAt(spawnPoint);

        foreach (var listener in listeners)
        {
            listener.onPlayerRespawnInThisCheckpoint(this, player);
        }
    }

    public virtual void AssignObjectToCheckPoint(IMPlayerRespawnListener listener)
    {
        listeners.Add(listener);
    }
}
