using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }

    [Header("Prefabs")]
    public CharacterBehavior PlayerPrefab;
    public CheckPoint DebugSpawn;
    public TimeSpan RunningTime { get { return DateTime.UtcNow - started; } }

    [Space(10)]
    [Header("Intro and Outro durations")]
    public float IntroFadeDuration = 1f;
    public float OutroFadeDuration = 1f;

    protected CharacterBehavior player;
    protected List<CheckPoint> checkpoints;
    protected int currentCheckPointIndex;
    protected DateTime started;
    protected int savedPoints;
    protected CameraController cameraController;

    public virtual void Awake()
    {
        Instance = this;
        if (PlayerPrefab != null)
        {
            player = (CharacterBehavior)Instantiate(PlayerPrefab, new Vector3(0, 0, 0), Quaternion.identity);
            GameManager.Instance.Player = player;
        }
    }

    public virtual void Start()
    {
        savedPoints = GameManager.Instance.Points;
        checkpoints = FindObjectsOfType<CheckPoint>().OrderBy(t => t.transform.position.x).ToList();
        currentCheckPointIndex = checkpoints.Count > 0 ? 0 : -1;
        started = DateTime.UtcNow;

        cameraController = FindObjectOfType<CameraController>();

        var listeners = FindObjectsOfType<MonoBehaviour>().OfType<IPlayerRespawnListener>();
        foreach (var listener in listeners)
        {
            for (var i = checkpoints.Count - 1; i >= 0; i--)
            {
                var distance = ((MonoBehaviour)listener).transform.position.x - checkpoints[i].transform.position.x;
                if (distance < 0)
                    continue;

                checkpoints[i].AssignObjectToCheckPoint(listener);
                break;
            }
        }

        if (GUIManager.Instance != null)
        {
            GUIManager.Instance.SetLevelName(Application.loadedLevelName);

            GUIManager.Instance.FaderOn(false, IntroFadeDuration);
        }

#if UNITYEDITOR
        if (DebugSpawn!= null)
		{
			DebugSpawn.SpawnPlayer(player);
		}
		else if (currentCheckPointIndex != -1)
		{
			checkpoints[currentCheckPointIndex].SpawnPlayer(player);
		}
#else
        if (currentCheckPointIndex != -1)
        {
            checkpoints[currentCheckPointIndex].SpawnPlayer(player);
        }
#endif
    }

    public virtual void Update()
    {
        var isAtLastCheckPoint = currentCheckPointIndex + 1 >= checkpoints.Count;
        if (isAtLastCheckPoint)
            return;

        var distanceToNextCheckPoint = checkpoints[currentCheckPointIndex + 1].transform.position.x - player.transform.position.x;
        if (distanceToNextCheckPoint >= 0)
            return;

        checkpoints[currentCheckPointIndex].PlayerLeftCheckPoint();

        currentCheckPointIndex++;
        checkpoints[currentCheckPointIndex].PlayerHitCheckPoint();

        savedPoints = GameManager.Instance.Points;
        started = DateTime.UtcNow;
    }

    public virtual void GotoLevel(string levelName)
    {
        if (GUIManager.Instance != null)
        {
            GUIManager.Instance.FaderOn(true, OutroFadeDuration);
        }
        StartCoroutine(GotoLevelCo(levelName));
    }

    protected virtual IEnumerator GotoLevelCo(string levelName)
    {
        if (player != null)
        {
            player.Disable();
        }


        if (Time.timeScale > 0.0f)
        {
            yield return new WaitForSeconds(OutroFadeDuration);
        }
        GameManager.Instance.UnPause();

        if (string.IsNullOrEmpty(levelName))
            Application.LoadLevel("StartScreen");
        else
            Application.LoadLevel(levelName);

    }

    public virtual void KillPlayer()
    {
        StartCoroutine(KillPlayerCo());
    }

    protected virtual IEnumerator KillPlayerCo()
    {
        player.Kill();
        cameraController.FollowsPlayer = false;
        yield return new WaitForSeconds(2f);

        cameraController.FollowsPlayer = true;
        if (currentCheckPointIndex != -1)
            checkpoints[currentCheckPointIndex].SpawnPlayer(player);

        started = DateTime.UtcNow;
        GameManager.Instance.SetPoints(savedPoints);
    }
}

