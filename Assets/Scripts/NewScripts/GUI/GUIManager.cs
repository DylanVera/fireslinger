﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GUIManager : MonoBehaviour
{
    public GameObject HUD;
    public GameObject PauseScreen;
    public GameObject TimeSplash;
    public GameObject Buttons;
    public GameObject Pad;
    public Text PointsText;
    public Text LevelText;
    public Image Fader;
    public GameObject JetPackBar;

    protected static GUIManager instance;

    public static GUIManager Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType<GUIManager>();
            return instance;
        }
    }

    protected virtual void Start()
    {
        RefreshPoints();

    }

    public virtual void SetHUDActive(bool state)
    {
        if (HUD != null)
        {
            HUD.SetActive(state);
        }
        if (PointsText != null)
        {
            PointsText.enabled = state;
        }
        if (LevelText != null)
        {
            LevelText.enabled = state;
        }
    }

    public virtual void SetAvatarActive(bool state)
    {
        if (HUD != null)
        {
            HUD.SetActive(state);
        }
    }

    public virtual void SetMobileControlsActive(bool state)
    {
        Pad.SetActive(state);
        Buttons.SetActive(state);
    }

    public virtual void SetPause(bool state)
    {
        if (PauseScreen != null)
        {
            PauseScreen.SetActive(state);
        }
    }

    public virtual void SetJetpackBar(bool state)
    {
        if (JetPackBar != null)
        {
            JetPackBar.SetActive(state);
        }
    }

    public virtual void SetTimeSplash(bool state)
    {
        if (TimeSplash != null)
        {
            TimeSplash.SetActive(state);
        }
    }

    public virtual void RefreshPoints()
    {
        if (PointsText != null)
        {
            PointsText.text = "$" + GameManager.Instance.Points.ToString("000000");
        }
    }

    public virtual void SetLevelName(string name)
    {
        if (LevelText != null)
        {
            LevelText.text = name;
        }
    }

    public virtual void FaderOn(bool state, float duration)
    {
        if (Fader != null)
        {
            Fader.gameObject.SetActive(true);
            if (state)
                StartCoroutine(Tools.FadeImage(Fader, duration, new Color(0, 0, 0, 1f)));
            else
                StartCoroutine(Tools.FadeImage(Fader, duration, new Color(0, 0, 0, 0f)));
        }
    }


}
