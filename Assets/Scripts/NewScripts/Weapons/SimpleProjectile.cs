﻿using UnityEngine;
using System.Collections;

public class SimpleProjectile : Projectile, CanTakeDamage
{
    public int Damage;
    public GameObject DestroyedEffect;
    public int PointsToGiveToPlayer;
    public float TimeToLive;

    protected virtual void Update()
    {
      
        if ((TimeToLive -= Time.deltaTime) <= 0)
        {
            DestroyProjectile();
            return;
        }
     
        transform.Translate(Direction * ((Mathf.Abs(InitialVelocity.x) + Speed) * Time.deltaTime), Space.World);
    }

    public virtual void TakeDamage(int damage, GameObject instigator, string tag)
    {
        if (PointsToGiveToPlayer != 0)
        {
            var projectile = instigator.GetComponent<Projectile>();
            if (projectile != null && projectile.Owner.GetComponent<CharacterBehavior>() != null)
            {
                GameManager.Instance.AddPoints(PointsToGiveToPlayer);
            }
        }

        DestroyProjectile();
    }

    protected override void OnCollideOther(Collider2D collider)
    {
        DestroyProjectile();
    }

    protected override void OnCollideTakeDamage(Collider2D collider, CanTakeDamage takeDamage)
    {
        takeDamage.TakeDamage(Damage, gameObject, Owner.tag);
        DestroyProjectile();
    }

    protected virtual void DestroyProjectile()
    {
        if (DestroyedEffect != null)
        {
            Instantiate(DestroyedEffect, transform.position, transform.rotation);
        }

        Destroy(gameObject);
    }
}
