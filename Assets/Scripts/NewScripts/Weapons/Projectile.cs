using UnityEngine;
using System.Collections;

public abstract class Projectile : MonoBehaviour
{
    public float Speed;
    public LayerMask CollisionMask;
    public GameObject Owner { get; private set; }
    public Vector2 Direction { get; private set; }
    public Vector2 InitialVelocity { get; private set; }


    public virtual void Initialize(GameObject owner, Vector2 direction, Vector2 initialVelocity)
    {
        transform.right = direction;
        Owner = owner;
        Direction = direction;
        InitialVelocity = initialVelocity;

        OnInitialized();
    }

    protected virtual void OnInitialized()
    {

    }


    protected virtual void OnTriggerEnter2D(Collider2D collider)
    {
        if ((CollisionMask.value & (1 << collider.gameObject.layer)) == 0)
        {
            OnNotCollideWith(collider);
            return;
        }

        var isOwner = collider.gameObject == Owner;
        if (isOwner)
        {
            OnCollideOwner();
            return;
        }

        var takeDamage = (CanTakeDamage)collider.GetComponent(typeof(CanTakeDamage));
        if (takeDamage != null)
        {
            OnCollideTakeDamage(collider, takeDamage);
            return;
        }

        OnCollideOther(collider);
    }

    protected virtual void OnNotCollideWith(Collider2D collider)
    {

    }

    protected virtual void OnCollideOwner()
    {

    }

    protected virtual void OnCollideTakeDamage(Collider2D collider, CanTakeDamage takeDamage)
    {

    }

    protected virtual void OnCollideOther(Collider2D collider)
    {

    }

}
