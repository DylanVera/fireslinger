﻿using UnityEngine;
using System.Collections;

public class AIShoot : MonoBehaviour
{
    public float FireRate = 1;
    public Projectile Projectile;
    public float ShootDistance = 10f;

    protected float canFireIn;
    protected Vector2 direction;
    protected Vector2 directionLeft;
    protected Vector2 directionRight;
    protected PlayerController controller;

    protected virtual void Start()
    {
        directionLeft = new Vector2(-1, 0);
        directionRight = new Vector2(1, 0);
        controller = GetComponent<PlayerController>();
    }

    /// Every frame, check for the player and try and kill it
    protected virtual void FixedUpdate()
    {
        // fire cooldown
        if ((canFireIn -= Time.deltaTime) > 0)
        {
            return;
        }

        // determine the direction of the AI
        if (transform.localScale.x < 0)
        {
            direction = -directionLeft;
        }
        else
        {
            direction = -directionRight;
        }

        // we cast a ray in front of the agent to check for a Player
        Vector2 raycastOrigin = new Vector2(transform.position.x, transform.position.y - (transform.localScale.y / 2));
        RaycastHit2D raycast = Physics2D.Raycast(raycastOrigin, direction, ShootDistance, 1 << LayerMask.NameToLayer("Player"));
        if (!raycast)
            return;

        // if the ray has hit the player, we fire a projectile
        Projectile projectile = (Projectile)Instantiate(Projectile, transform.position, transform.rotation);
        projectile.Initialize(gameObject, direction, controller.Speed);
        canFireIn = FireRate;
    }
}
