﻿using UnityEngine;
using System.Collections;

public class AIWalk : MonoBehaviour, IPlayerRespawnListener
{
    public float Speed;
    public bool GoesRightInitially = true;
    public bool AvoidFalling = false;

    protected PlayerController controller;
    protected Vector2 direction;
    protected Vector2 startPosition;
    protected Vector2 initialDirection;
    protected Vector3 initialScale;
    protected Vector3 holeDetectionOffset;
    protected float holeDetectionRayLength = 1f;

    protected virtual void Awake()
    {
        controller = GetComponent<PlayerController>();
        startPosition = transform.position;
        direction = GoesRightInitially ? Vector2.right : -Vector2.right;
        if(GoesRightInitially)
        {
            Flip();
        }
        initialDirection = direction;
        initialScale = transform.localScale;
        holeDetectionOffset = new Vector3(1, 0, 0);
    }

    protected virtual void Update()
    {
        controller.SetHorizontalForce(direction.x * Speed);
        CheckForWalls();
        if (AvoidFalling)
        {
            CheckForHoles();
        }
    }

    protected void Flip()
    {
        Vector3 theScale = gameObject.transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
    protected virtual void CheckForWalls()
    {
        if ((direction.x < 0 && controller.State.IsCollidingLeft) || (direction.x > 0 && controller.State.IsCollidingRight))
        {
            ChangeDirection();
        }
    }

    protected virtual void CheckForHoles()
    {
        Vector2 raycastOrigin = new Vector2(transform.position.x + direction.x * holeDetectionOffset.x, transform.position.y + holeDetectionOffset.y - (transform.localScale.y / 2));
        RaycastHit2D raycast = Tools.RayCast(raycastOrigin, Vector2.down, holeDetectionRayLength, 1 << LayerMask.NameToLayer("Platforms"), true, Color.yellow);
        if (!raycast)
        {
            ChangeDirection();
        }
    }

    protected virtual void ChangeDirection()
    {
        direction = -direction;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
    }

    public virtual void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, CharacterBehavior player)
    {
        direction = initialDirection;
        transform.localScale = initialScale;
        transform.position = startPosition;
        gameObject.SetActive(true);
    }

}
