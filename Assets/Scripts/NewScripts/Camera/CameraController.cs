using UnityEngine;
using System.Collections;
[RequireComponent(typeof(Camera))]

public class CameraController : MonoBehaviour
{
    public bool FollowsPlayer { get; set; }

    [Space(10)]
    [Header("Distances")]
    public float HorizontalLookDistance = 3;
    public Vector3 CameraOffset;
    public float LookAheadTrigger = 0.1f;
    public float ManualUpDownLookDistance = 3;


    [Space(10)]
    [Header("Movement Speed")]
    public float ResetSpeed = 0.5f;
    public float CameraSpeed = 0.3f;

    [Space(10)]
    [Header("Camera Zoom")]
    [Range(1, 20)]
    public float MinimumZoom = 5f;
    [Range(1, 20)]
    public float MaximumZoom = 10f;
    public float ZoomSpeed = 0.4f;


    protected Transform target;
    protected PlayerController targetController;
    protected LevelLimits levelBounds;

    protected float xMin;
    protected float xMax;
    protected float yMin;
    protected float yMax;

    protected float offsetZ;
    protected Vector3 lastTargetPosition;
    protected Vector3 currentVelocity;
    protected Vector3 lookAheadPos;

    protected float shakeIntensity;
    protected float shakeDecay;
    protected float shakeDuration;

    protected float currentZoom;
    protected Camera camera;

    protected Vector3 lookDirectionModifier = new Vector3(0, 0, 0);

    protected virtual void Start()
    {
        camera = GetComponent<Camera>();


        FollowsPlayer = true;
        currentZoom = MinimumZoom;


        target = GameManager.Instance.Player.transform;
        if (target.GetComponent<PlayerController>() == null)
            return;
        targetController = target.GetComponent<PlayerController>();
        levelBounds = GameObject.FindGameObjectWithTag("LevelBounds").GetComponent<LevelLimits>();

        lastTargetPosition = target.position;
        offsetZ = (transform.position - target.position).z;
        transform.parent = null;

        //lookDirectionModifier=new Vector3(0,0,0);

        Zoom();
    }

    protected virtual void FixedUpdate()
    {
        if (!FollowsPlayer)
            return;

        Zoom();

        float xMoveDelta = (target.position - lastTargetPosition).x;

        bool updateLookAheadTarget = Mathf.Abs(xMoveDelta) > LookAheadTrigger;

        if (updateLookAheadTarget)
        {
            lookAheadPos = HorizontalLookDistance * Vector3.right * Mathf.Sign(xMoveDelta);
        }
        else
        {
            lookAheadPos = Vector3.MoveTowards(lookAheadPos, Vector3.zero, Time.deltaTime * ResetSpeed);
        }

        Vector3 aheadTargetPos = target.position + lookAheadPos + Vector3.forward * offsetZ + lookDirectionModifier + CameraOffset;

        Vector3 newCameraPosition = Vector3.SmoothDamp(transform.position, aheadTargetPos, ref currentVelocity, CameraSpeed);

        Vector3 shakeFactorPosition = new Vector3(0, 0, 0);

        if (shakeDuration > 0)
        {
            shakeFactorPosition = Random.insideUnitSphere * shakeIntensity * shakeDuration;
            shakeDuration -= shakeDecay * Time.deltaTime;
        }
        newCameraPosition = newCameraPosition + shakeFactorPosition;

        float posX = Mathf.Clamp(newCameraPosition.x, xMin, xMax);
        float posY = Mathf.Clamp(newCameraPosition.y, yMin, yMax);
        float posZ = newCameraPosition.z;

        transform.position = new Vector3(posX, posY, posZ);

        lastTargetPosition = target.position;
    }

    protected virtual void Zoom()
    {

        float characterSpeed = Mathf.Abs(targetController.Speed.x);
        float currentVelocity = 0f;

        currentZoom = Mathf.SmoothDamp(currentZoom, (characterSpeed / 10) * (MaximumZoom - MinimumZoom) + MinimumZoom, ref currentVelocity, ZoomSpeed);

        camera.orthographicSize = currentZoom;
        GetLevelBounds();
    }

    protected virtual void GetLevelBounds()
    {
        float cameraHeight = Camera.main.orthographicSize * 2f;
        float cameraWidth = cameraHeight * Camera.main.aspect;

        xMin = levelBounds.LeftLimit + (cameraWidth / 2);
        xMax = levelBounds.RightLimit - (cameraWidth / 2);
        yMin = levelBounds.BottomLimit + (cameraHeight / 2);
        yMax = levelBounds.TopLimit - (cameraHeight / 2);
    }

    public virtual void Shake(Vector3 shakeParameters)
    {
        shakeIntensity = shakeParameters.x;
        shakeDuration = shakeParameters.y;
        shakeDecay = shakeParameters.z;
    }

    public virtual void LookUp()
    {
        lookDirectionModifier = new Vector3(0, ManualUpDownLookDistance, 0);
    }

    public virtual void LookDown()
    {
        lookDirectionModifier = new Vector3(0, -ManualUpDownLookDistance, 0);
    }

    public virtual void ResetLookUpDown()
    {
        lookDirectionModifier = new Vector3(0, 0, 0);
    }


}