using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFollow : MonoBehaviour 
{
	public enum FollowType
	{
		MoveTowards,
		Lerp
	}
	public FollowType Type = FollowType.MoveTowards;
	public PathDefinition Path;
	public float Speed = 1;
	public float MaxDistanceToGoal = .1f;
	public Vector3 CurrentSpeed;

    protected IEnumerator<Transform> currentPoint;
	public BoxCollider2D boxCollider { get; private set; }

    protected virtual void Start ()
	{
		if(Path == null)
		{
			Debug.LogError("Path Cannot be null", gameObject);
			return;
		}
		
		if (transform.GetComponent<BoxCollider2D>()!=null)
			boxCollider=transform.GetComponent<BoxCollider2D>();

		currentPoint = Path.GetPathEnumerator();
		currentPoint.MoveNext();
		
		if(currentPoint.Current == null)
			return;

		transform.position = currentPoint.Current.position;
	}

    protected virtual void FixedUpdate ()
	{
		if(currentPoint == null || currentPoint.Current == null)
			return;
			
		Vector3 initialPosition=transform.position;
		
		if(Type == FollowType.MoveTowards)
			transform.position = Vector3.MoveTowards(transform.position, currentPoint.Current.position, Time.deltaTime * Speed);
		else if(Type == FollowType.Lerp)
			transform.position = Vector3.Lerp(transform.position, currentPoint.Current.position, Time.deltaTime * Speed);
		
		var distanceSquared = (transform.position - currentPoint.Current.position).sqrMagnitude;
		if(distanceSquared < MaxDistanceToGoal * MaxDistanceToGoal)
			currentPoint.MoveNext();
			
		Vector3 finalPosition=transform.position;
		CurrentSpeed=(finalPosition-initialPosition)/Time.deltaTime;
	}
}
